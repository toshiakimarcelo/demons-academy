﻿using UnityEngine;
using System.Collections;

public class CameraLimitController : MonoBehaviour {
	
	public string tagCameraLimit;
	public DeathType deathType;

	void OnTriggerExit2D  (Collider2D other) {
		if (other.tag == tagCameraLimit) {
			if (transform.position.y < other.transform.position.y) {
				Death death = GetComponent<Death> ();
				death.KillCharacter (deathType);
				SoundController.PlaySound (SoundType.FallDeath);

			}
		}
	}
}
