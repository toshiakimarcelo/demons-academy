﻿using UnityEngine;
using System.Collections;

public class Cheat : MonoBehaviour {

	public GameObject menu;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.K) && tag == "Player") {
			this.tag = "Cheat";
			GetComponent<Jump> ().jumpCount = 50;
		} 
		else if (Input.GetKeyDown (KeyCode.K) && tag == "Cheat") {
			this.tag = "Player";
			GetComponent<Jump> ().jumpCount = 2;
		}
		if (Input.GetKey (KeyCode.F5)) {
			if (menu != null) {
				PlayerPrefs.DeleteAll ();
				if (!PlayerPrefs.HasKey ("res")) {
					PlayerPrefs.SetInt ("res", 1);
					PlayerPrefs.SetInt ("resWidth", Screen.width); 
					PlayerPrefs.SetInt ("resHeight", Screen.height); 
				}
				if (!PlayerPrefs.HasKey("level")) PlayerPrefs.SetInt ("level", 1);
				if (!PlayerPrefs.HasKey("FirstTimePlaying")) PlayerPrefs.SetString ("FirstTimePlaying", "true");
				menu.GetComponent<Menu> ().ReadErase ();
				menu.GetComponent<Menu> ().Read ();
			}
		}
		if (Input.GetKey (KeyCode.F6)) {
			PlayerPrefs.SetInt ("level", 33);
			PlayerPrefs.SetString ("FirstTimePlaying", "false");
			if (menu != null)
				menu.GetComponent<Menu> ().Read ();
		}
	}
}
