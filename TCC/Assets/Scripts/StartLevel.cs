﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartLevel : MonoBehaviour {

	public GameObject canvas;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartLevelButton(int value) {
		canvas.SetActive(true);
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		//if (value < 16)	SoundController.PlayBGM (SoundTypeBGM.World1);
		SceneManager.LoadScene(value);

	}
}
