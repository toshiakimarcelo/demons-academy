﻿using UnityEngine;
using System.Collections;

public class TriggerStudentConcha2 : MonoBehaviour {

	public GameObject student;
	public GameObject explosion;
	private bool explode;

	void OnTriggerEnter2D(Collider2D other){
		if (!explode) {
			if (other.name == student.name) {
				explosion.transform.position = student.transform.position;
				explosion.SetActive (true);
				GameObject.Destroy (other.gameObject);
				explode = true;
				SoundController.PlaySoundStudent (SoundStudentType.TreeDeath, other.transform.position);
			}
		}
	}
}
