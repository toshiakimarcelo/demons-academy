﻿using UnityEngine;
using System.Collections;

public class EnemyBitter : MonoBehaviour {

	public string tagPlayer;
	public DeathType deathType;

	public float lenghtX;
	public float lenghtY;

	public float speedIdle;
	public float speedChase;

	public bool startAgain;

	public Directions directions = Directions.Right;

	private Vector2 initialPos;
	private Rigidbody2D body2d;
	private SpriteRenderer sprite;
	public bool isDown;

	void Awake() {
		initialPos = transform.position;
		body2d = GetComponent<Rigidbody2D> ();
		sprite = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (startAgain) {
			transform.position = Vector2.MoveTowards (transform.position, initialPos, speedChase * Time.deltaTime);
			if (transform.position.x == initialPos.x && transform.position.y == initialPos.y) {
				startAgain = false;
				body2d.velocity = Vector2.zero;
			}
		} 
		else {
			if (transform.position.y >= initialPos.y + lenghtY) {
				isDown = true;
			} else if (transform.position.y <= initialPos.y - lenghtY) {
				isDown = false;
			}

			body2d.velocity = new Vector2 (0, speedIdle * Time.deltaTime * (isDown == false ? 1 : -1));
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == tagPlayer) {
			Death death = other.GetComponent<Death> ();

			death.KillCharacter (deathType);

			SoundController.PlaySound (SoundType.ImmunityDeath);
		}
	}
}
