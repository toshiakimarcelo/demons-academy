﻿using UnityEngine;
using System.Collections;

public class WallDow : MonoBehaviour {

	private Vector2 startPos;
	// Use this for initialization

	void Start () {
		startPos = transform.position;
	}

	// Update is called once per frame
	void Update () {
		if (this.transform.position.y > startPos.y - 2)
			this.transform.position = Vector2.MoveTowards (this.transform.position, new Vector2 (this.transform.position.x, this.transform.position.y - 2),4*Time.deltaTime);
	}
}
