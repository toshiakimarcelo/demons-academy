﻿using UnityEngine;
using System.Collections;

public class TriggerStudentFada : MonoBehaviour {

	public GameObject student;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.name == student.name) {
			student.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
		}
	}
}
