﻿using UnityEngine;
using System.Collections;

public class TriggerEnemyPlatform : MonoBehaviour {

	public EnemyLaser enemyLaser;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay2D (Collider2D other) {
		if (other.tag == "Player") {
			enemyLaser.triggered = true;
		} 
		else if (other.gameObject == null) {
			enemyLaser.triggered = false;
		}
	}
}
