﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public enum AudumblaType {Normal, Fast, Sprinkler, Dead}; 

public class Audumbla : MonoBehaviour {

	public AudumblaType audumblaType;

	public AudumblaLife audumblaLife;

	public GameObject[] lasers;
	public float cdLaser;

	private Transform player;

	private SpriteRenderer sprite;

	private bool canGo = true;

	public bool triggered;

	public GameObject explosionAudumbla;
	public bool destroy = false;


	float angle = 0;

	// Use this for initialization
	void Awake() {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
	}

	// Update is called once per frame
	void Update () {
		if (audumblaType == AudumblaType.Normal) {
			Vector3 dir = player.position - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

			if (GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent (0) == null) GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "TetaBossVacaGIdleNormal", true);

			if (canGo) {
				Invoke ("CooldownLaser", cdLaser);
				canGo = false;
			}
		}
		if (audumblaType == AudumblaType.Sprinkler) {
			angle += 100 * Time.deltaTime;
			transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
			if (canGo) {
				Invoke ("EnemyLaserGo", .125f);
				canGo = false;
			}

		}
		if (audumblaType == AudumblaType.Fast) {
			Vector3 dir = player.position - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

			if (GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent (0) == null) GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "TetaBossVacaGIdleNormal", true);

			if (canGo) {
				Invoke ("CooldownLaser", cdLaser);
				canGo = false;
			}
		}

		if (destroy) {
			explosionAudumbla.transform.position = this.transform.position;
			explosionAudumbla.SetActive (true);
			GameObject.Destroy (this.gameObject);
			SoundController.PlaySoundBoss (SoundBossType.BossEffect6);
		}

		if (Input.GetKey (KeyCode.F1) && player.GetComponent<Cheat>().enabled) {
			destroy = true;
			Damage ();
		}
	}

	void CooldownLaser() {
			Invoke ("EnemyLaserGo", .75f);
			GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation(0, "TetaBossVacaGAtaqueNormal", false);
			triggered = false;
	}

	void EnemyLaserGo() {
		for (int i = 0; i < lasers.Length; i++) {
			if (lasers [i].GetComponent<AudumblaLaser> ().canGo) {

				lasers [i].transform.position = transform.position + transform.right*3f;
				//lasers [i].transform.position = new Vector2 (GetComponent<BoxCollider2D> ().bounds.max.x, GetComponent<BoxCollider2D> ().bounds.max.y);
				lasers [i].transform.rotation = transform.rotation;
				lasers [i].GetComponent<AudumblaLaser> ().StartLaser ();
				lasers [i].GetComponent<AudumblaLaser> ().canGo = false;
				break;
			}
		}
		if (audumblaType == AudumblaType.Sprinkler) {
			SoundController.PlaySoundBoss (SoundBossType.Tits3);
		}
		else if (audumblaType == AudumblaType.Normal) {
			SoundController.PlaySoundBoss (SoundBossType.Tits2);
		}
		else if (audumblaType == AudumblaType.Fast) {
			SoundController.PlaySoundBoss (SoundBossType.Tits1);
		}
		canGo = true;
	}

	public void Damage() {
		audumblaLife.value++;
	}


		
}
