﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUDImage : MonoBehaviour {

	public Sprite hud1;
	public Sprite hud2;

	public Image image;
	private RectTransform rt;

	public Sprite detalhe1;
	public Sprite detalhe2;

	public GameObject detalhe;
	private Image detalheImg;
	private RectTransform detalheRT;

	public Text currentLevel;

	void Start (){
		rt = GetComponent<RectTransform>();
		detalheImg = detalhe.GetComponent<Image>();
		detalheRT = detalhe.GetComponent<RectTransform>();
	}

	void Update () {
		if (SceneManager.GetActiveScene ().buildIndex <= 17) {
			image.sprite = hud1;
			rt.anchoredPosition = new Vector3 (0,-5.6f,0);
			rt.sizeDelta = new Vector2 (200.8f, 47.1f);

			detalheImg.sprite = detalhe1;
			detalheRT.anchoredPosition = new Vector3 (5,-10.2f,0);
			detalheRT.sizeDelta = new Vector2 (11.6f,11.6f);
		} 
		else if (SceneManager.GetActiveScene ().buildIndex > 17 && SceneManager.GetActiveScene ().buildIndex <= 33) {
			image.sprite = hud2;
			rt.anchoredPosition = new Vector3 (0,-5.9f,0);
			rt.sizeDelta = new Vector2 (194.9f,41);

			detalheImg.sprite = detalhe2;
			detalheRT.anchoredPosition = new Vector3 (4.06f,-25.54f,0);
			detalheRT.sizeDelta = new Vector2 (32.34f,26.22f);
		}

		currentLevel.text = SceneManager.GetActiveScene().name;
	}
}
