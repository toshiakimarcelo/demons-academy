﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class EnemyPoisonTEMP : MonoBehaviour {

	public string tagPlayer;

	public DeathType deathType;

	[HideInInspector] public bool alreadyDead;

	void Update() {
		if (!alreadyDead && GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent (0) == null) {
			GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, "InimigoVenenosoGeloIdle", true);
		}
		if (GetComponentInChildren<SkeletonAnimation> ().state.ToString() == "InimigoVenenosoGeloDormir"){
			GetComponentInChildren<SkeletonAnimation> ().state.AddAnimation(0, "InimigoVenenosoGeloDormirIdle", true, 0);
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Resource") {
			alreadyDead = true;
		}
		if (other.tag == tagPlayer && !alreadyDead) {
			GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, "InimigoVenenosoGeloDormir", false);

			SoundController.PlaySoundEnemy (SoundEnemyType.EnemyPoison, this.transform.position);

			Death death = other.GetComponent<Death> ();

			death.KillCharacter (deathType);
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == "Resource") {
			alreadyDead = false;
			GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, "InimigoVenenosoGeloAcordar", false);
		}
	}
}