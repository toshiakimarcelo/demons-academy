﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class AutoTypeToshi : MonoBehaviour {
	
	public float letterPause = 0.2f;

//	public AudioClip textSound;
//	public AudioSource audioSource1;
//	public AudioSource audioSource2;

//	public AudioClip[] cutsceneSounds;
	
//	public GameObject continuaBotao;

	public Text textName;
	public Text text;

	[HideInInspector] public string[] name;
	[HideInInspector] public string[] message;
	[HideInInspector] public string[] image;

	public GameObject imageChar1;
	public GameObject imageChar2;
	public GameObject imageChar3;

	public string[] name1;
	public string[] talk1;
	public string[] image1;

	public GameObject panel;

	public GameObject player;

	public int i = 0;

	private bool firsTime; 

	void Start ()
	{
//		player = GameObject.FindWithTag ("Player");
//		message[0] = text.text;
//		text.text = "";

		if (PlayerPrefs.HasKey ("tutoStart"))
			firsTime = (PlayerPrefs.GetInt ("tutoStart") == 1) ? true : false;
		if (SceneManager.GetActiveScene ().buildIndex == 1 && !firsTime) {
			PlayerPrefs.SetInt ("tutoStart", 1);

			player.GetComponent<Walk> ().enabled = false;
			player.GetComponent<Jump> ().enabled = false;
			player.GetComponent<FaceDirection> ().enabled = false;
			name = name1;
			message = talk1;
			image = image1;
			panel.SetActive (true);
			StartCoroutine (TypeText ());
			firsTime = true;
		}
	}
	
	public IEnumerator TypeText ()
	{

		foreach (char letter in message[i].ToCharArray())
		{
			text.text += letter;

			if(image [i] == "2") SoundController.PlaySoundDialogue ();
			else SoundController.PlaySoundDialogueThyrone ();
			yield return new WaitForSeconds (letterPause);
		}
	}

	public void Update()
	{
		

		player.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, player.GetComponent<Rigidbody2D> ().velocity.y);
		player.GetComponent<Walk> ().enabled = false;
		player.GetComponent<Jump> ().enabled = false;
		player.GetComponent<FaceDirection> ().enabled = false;

		textName.text = name [i];
		if (image [i] == "1") {
			imageChar1.SetActive (true);
			imageChar2.SetActive (false);
			imageChar3.SetActive (false);
		} else if (image [i] == "2") {
			imageChar1.SetActive (false);
			imageChar2.SetActive (true);
			imageChar3.SetActive (false);
		} else if (image[i] == "3") {
			imageChar1.SetActive (false);
			imageChar2.SetActive (false);
			imageChar3.SetActive (true);
		}

		if (Input.GetKeyDown (KeyCode.Space) && i < message.Length && text.text != message [i]) {
			Debug.Log ("1");
			StopAllCoroutines ();
			text.text = message [i];
		} 
		else if (Input.GetKeyDown (KeyCode.Space) && i < message.Length-1 && text.text == message [i]) {
			StopAllCoroutines ();
			i++;
			text.text = "";
			StartCoroutine (TypeText ());
		} 
		else if (Input.GetKeyDown (KeyCode.Space) && i == message.Length-1 && text.text == message [i]) {
			StopAllCoroutines ();
			imageChar1.SetActive (false);
			imageChar2.SetActive (false);
			imageChar3.SetActive (false);
			panel.SetActive (false);
			this.enabled = false;
			player.GetComponent<Walk> ().enabled = true;
			player.GetComponent<Jump> ().enabled = true;
			player.GetComponent<FaceDirection> ().enabled = true;
		}
	}


}