﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class EnemyPoison : MonoBehaviour {

	public string tagPlayer;

	public DeathType deathType;

	[HideInInspector] public bool alreadyDead;

	void Update() {
		if (!alreadyDead && GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent (0) == null) {
			GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, "InimigoVenenosoIdle", true);
		}

		if (alreadyDead && GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent (0) == null) {
			GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, "InimigoVenenosoDormirIdle", true);
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Stick") {
			alreadyDead = true;
		}
		if (other.tag == tagPlayer && !alreadyDead) {
			GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, "InimigoVenenosoDormir", false);

            alreadyDead = true;

            SoundController.PlaySoundEnemy (SoundEnemyType.EnemyPoison, this.transform.position);

			Death death = other.GetComponent<Death> ();

			death.KillCharacter (deathType);
		}

	}

	void OnTriggerExit2D (Collider2D other) {
	if (other.tag == "Stick") {
		alreadyDead = false;
		GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, "InimigoVenenosoAcordar", false);
		}
	}
}