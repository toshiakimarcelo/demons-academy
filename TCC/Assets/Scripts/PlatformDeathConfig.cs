﻿using UnityEngine;
using System.Collections;

public class PlatformDeathConfig : MonoBehaviour {

	// Use this for initialization
	void Start() {
		GetComponentInParent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
	}

}
