﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class StickDeathConfig : MonoBehaviour {

	private EnemyPoison enemyPoison;
	void Start() {
		GetComponentInParent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
	}

	void Update() {
		if (GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent (0) == null) {
			transform.localScale = new Vector3 (transform.localScale.x, 3, 1);
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "EnemyPoison") {
			enemyPoison = other.GetComponent<EnemyPoison> ();
            SoundController.PlaySound(SoundType.StickDeath);
            transform.localScale = new Vector3(other.transform.localScale.x, 2, 1);
//            if (transform.localScale.x == other.transform.localScale.x) {
//				transform.localScale = new Vector3 (other.transform.localScale.x, 2, 1);
//			} 
//			else {
//				transform.localScale = new Vector3 (-1, 2, 1);
//			}
			transform.position = new Vector2 (other.transform.position.x, other.transform.position.y);
		}
	}

	void OnDestroy() {
		enemyPoison.alreadyDead = false;
		enemyPoison.GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0, "InimigoVenenosoAcordar", false);
	}
}
