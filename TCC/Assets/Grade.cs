﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Grade : MonoBehaviour {

	public Text text;

	public GradeCheckpoints gradeCheckpoints;
	public GradeTime gradeTime;

	public float time100Percent;

	public float checkpointDiscount;

	float percent;

	// Use this for initialization
	void Start () {
		float discount = 0;
		float percentValue;
		percentValue = gradeTime.time - time100Percent;
		percent = percentValue / time100Percent * 100;
		if (percent <= 3) {
			discount = 0;
		} 
		else if (percent > 3 && percent <= 7) {
			discount = 1;
		}
		else if (percent > 7 && percent <= 10) {
			discount = 2;
		}
		else if (percent > 10 && percent <= 13) {
			discount = 3;
		}
		else if (percent > 13 && percent <= 17) {
			discount = 4;
		}
		else if (percent > 17 && percent <= 20) {
			discount = 5;
		}
		else if (percent > 20 && percent <= 23) {
			discount = 6;
		}
		else if (percent > 23 && percent <= 27) {
			discount = 7;
		}
		else if (percent > 27 && percent <= 30) {
			discount = 8;
		}
		else if (percent > 30 && percent <= 33) {
			discount = 9;
		}
		else if (percent > 33 && percent <= 37) {
			discount = 10;
		}
		else if (percent > 37 && percent <= 40) {
			discount = 11;
		}
		else if (percent > 40) {
			discount = 12;
		}

		float grade = 13;
		grade -= discount;
		grade = grade - (gradeCheckpoints.checkpoint / checkpointDiscount);
		string gradeText;
		int gradeValue;

		if (grade > 12) {
			gradeText = "A+";
			gradeValue = 13;
		}
		else if (grade > 11) {
			gradeText = "A";
			gradeValue = 12;
		}
		else if (grade > 10) {
			gradeText = "A-";
			gradeValue = 11;
		}
		else if (grade > 9) {
			gradeText = "B+";
			gradeValue = 10;
		}
		else if (grade > 8) {
			gradeText = "B";
			gradeValue = 9;
		}
		else if (grade > 7) {
			gradeText = "B-";
			gradeValue = 8;
		}
		else if (grade > 6) {
			gradeText = "C+";
			gradeValue = 7;
		}
		else if (grade > 5) {
			gradeText = "C";
			gradeValue = 6;
		}
		else if (grade > 4) {
			gradeText = "C-";
			gradeValue = 5;
		}
		else if (grade > 3) {
			gradeText = "D+";
			gradeValue = 4;
		}
		else if (grade > 2) {
			gradeText = "D";
			gradeValue = 3;
		}
		else if (grade > 1) {
			gradeText = "D-";
			gradeValue = 2;
		}
		else if (grade > 0) {
			gradeText = "F";
			gradeValue = 1;
		}
		else {
			gradeText = "F";
			gradeValue = 1;
		}

		text.text = "Grade " + gradeText;
		gradeTime.arrived = true;

		if (grade > PlayerPrefs.GetInt ("GradeValue" + (SceneManager.GetActiveScene ().buildIndex -1).ToString()) || !PlayerPrefs.HasKey("GradeValue" + (SceneManager.GetActiveScene ().buildIndex -1).ToString())) {
			PlayerPrefs.SetString ("GradeText" + (SceneManager.GetActiveScene ().buildIndex -1).ToString(), gradeText);
			PlayerPrefs.SetInt ("GradeValue" + (SceneManager.GetActiveScene ().buildIndex -1).ToString(), gradeValue);
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
}
