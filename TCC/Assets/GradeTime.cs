﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GradeTime : MonoBehaviour {

	public float time;

	public Text text;
	public bool arrived;

	public Text textHud;

	// Use this for initialization
	void Start () {
		if (GameObject.Find ("GradeTimeHUD")) textHud = GameObject.Find ("GradeTimeHUD").GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!arrived) {
			time += Time.deltaTime;
			float m = Mathf.Floor(time / 60);
			float s = time - Mathf.Floor(time / 60)*60;
			//float h = time / (60 * 24);
			text.text = time.ToString ();
			text.text = string.Format ("{0:00}:{1:00}", m, s);
			if (textHud != null) textHud.text = string.Format ("{0:00}:{1:00}", m, s);
		}


	}
}
 