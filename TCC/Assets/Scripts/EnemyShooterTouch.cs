﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class EnemyShooterTouch : MonoBehaviour {

	public string tagPlayer;

	public DeathType deathType;

	private BoxCollider2D boxCollider;

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == tagPlayer) {
			Death death = other.GetComponent<Death> ();

			death.KillCharacter (deathType);
		}
	}
}
