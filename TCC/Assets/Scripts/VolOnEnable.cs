﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolOnEnable : MonoBehaviour {


	public AudioMixer audioMixer;

	public Slider master;
	public Slider bgm;
	public Slider sfx;
	//public Slider dialogue;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable() {
		if (PlayerPrefs.HasKey ("masterVol")) {
			audioMixer.SetFloat ("master", PlayerPrefs.GetFloat ("masterVol"));
			audioMixer.SetFloat ("bgm", PlayerPrefs.GetFloat ("bgmVol"));
			audioMixer.SetFloat ("sfx", PlayerPrefs.GetFloat ("sfxVol"));
			//audioMixer.SetFloat ("dialogue", PlayerPrefs.GetFloat ("dialogueVol"));

			master.value = PlayerPrefs.GetFloat ("masterVol");
			bgm.value = PlayerPrefs.GetFloat ("bgmVol");
			sfx.value = PlayerPrefs.GetFloat ("sfxVol");
			//dialogue.value = PlayerPrefs.GetFloat ("dialogueVol");
		}
	}
}
