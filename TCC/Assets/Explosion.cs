﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public GameObject[] arrayExplosion;

	// Use this for initialization
	void Start () {
		for (int i = 0; arrayExplosion.Length > i; i++) {
			float randomDir = Random.Range (0, 10);
			float random = 0;
			if (randomDir <= 5) {
				random = Random.Range (10, 15);
			}
			else {
				random = Random.Range (-15, -10);
			}

			arrayExplosion [i].GetComponent<Rigidbody2D> ().AddForce (new Vector2 (1*random, 1*random), ForceMode2D.Impulse);
			//arrayExplosion [i].GetComponent<Rigidbody2D> ().AddForceAtPosition (new Vector2 (10*random, 10*random), this.gameObject.transform.position);
			arrayExplosion [i].GetComponent<Rigidbody2D> ().AddTorque (1000);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
