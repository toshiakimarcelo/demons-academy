﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartLevelTEMP : MonoBehaviour {

	public GameObject canvas;
	public GameObject HUD;
	public GameObject loadingCanvas;

	private AsyncOperation ao;
	public GameObject loadingScreenBG;
	public Image progressBar;
	public Text loadingText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartMainMenu(int value)
	{
		SceneManager.LoadScene(value);
	}

	public void StarLevelButtonWithRealLoading(int value)
	{
		SoundController.PlayMenuSFX (SoundTypeMenu.Go);
		loadingCanvas.SetActive(true);
		canvas.SetActive(true);
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		StartCoroutine(LoadLevelSeijiReal(value));
	}

	IEnumerator LoadLevelSeijiReal(int value)
	{
		yield return new WaitForEndOfFrame();

		ao = SceneManager.LoadSceneAsync(value);
		ao.allowSceneActivation = false;

		while(!ao.isDone)
		{
			progressBar.fillAmount = ao.progress;

			if (ao.progress == 0.9f)
			{
				progressBar.fillAmount = 1f;
				ao.allowSceneActivation = true;
			}
			yield return null;
		}
	}

	public void LoadIntroScene(int value)
	{
		SoundController.PlayMenuSFX (SoundTypeMenu.Go);
		loadingCanvas.SetActive(true);
		canvas.SetActive(true);
		//HUD.SetActive (false);
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		StartCoroutine(LoadLevelSeijiReal(value));
	}
}
