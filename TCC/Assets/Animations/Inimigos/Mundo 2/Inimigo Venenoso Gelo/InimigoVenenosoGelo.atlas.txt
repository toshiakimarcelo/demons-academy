
InimigoVenenosoGelo.png
format: RGBA8888
filter: Linear,Linear
repeat: none
Cilios Direita Frente
  rotate: false
  xy: 918, 317
  size: 524, 777
  orig: 526, 779
  offset: 1, 1
  index: -1
Cilios Direita Meio
  rotate: false
  xy: 1444, 380
  size: 330, 714
  orig: 332, 716
  offset: 1, 1
  index: -1
Cilios Direita Tras
  rotate: true
  xy: 1776, 682
  size: 301, 701
  orig: 303, 703
  offset: 1, 1
  index: -1
Cilios Esquerda Frente
  rotate: true
  xy: 2760, 1506
  size: 524, 777
  orig: 526, 779
  offset: 1, 1
  index: -1
Cilios Esquerda Meio
  rotate: true
  xy: 1878, 985
  size: 330, 714
  orig: 332, 716
  offset: 1, 1
  index: -1
Cilios Esquerda Tras
  rotate: true
  xy: 1776, 379
  size: 301, 701
  orig: 303, 703
  offset: 1, 1
  index: -1
Cube de Gelo copiar
  rotate: false
  xy: 2479, 534
  size: 431, 449
  orig: 433, 451
  offset: 1, 1
  index: -1
Cube de Gelo copiar 10
  rotate: true
  xy: 3733, 739
  size: 374, 343
  orig: 376, 345
  offset: 1, 1
  index: -1
Cube de Gelo copiar 11
  rotate: true
  xy: 2479, 143
  size: 389, 410
  orig: 391, 412
  offset: 1, 1
  index: -1
Cube de Gelo copiar 12
  rotate: true
  xy: 3244, 1055
  size: 447, 471
  orig: 449, 473
  offset: 1, 1
  index: -1
Cube de Gelo copiar 13
  rotate: true
  xy: 2912, 608
  size: 429, 424
  orig: 431, 426
  offset: 1, 1
  index: -1
Cube de Gelo copiar 2
  rotate: false
  xy: 2912, 275
  size: 366, 331
  orig: 368, 333
  offset: 1, 1
  index: -1
Cube de Gelo copiar 3
  rotate: true
  xy: 3338, 637
  size: 416, 393
  orig: 418, 395
  offset: 1, 1
  index: -1
Cube de Gelo copiar 4
  rotate: true
  xy: 1237, 8
  size: 307, 277
  orig: 309, 279
  offset: 1, 1
  index: -1
Cube de Gelo copiar 6
  rotate: true
  xy: 1237, 8
  size: 307, 277
  orig: 309, 279
  offset: 1, 1
  index: -1
Cube de Gelo copiar 5
  rotate: true
  xy: 3539, 1504
  size: 526, 528
  orig: 528, 530
  offset: 1, 1
  index: -1
Cube de Gelo copiar 7
  rotate: true
  xy: 2760, 1039
  size: 465, 482
  orig: 467, 484
  offset: 1, 1
  index: -1
Cube de Gelo copiar 8
  rotate: false
  xy: 918, 2
  size: 317, 313
  orig: 319, 315
  offset: 1, 1
  index: -1
Cube de Gelo copiar 9
  rotate: false
  xy: 3717, 1115
  size: 372, 387
  orig: 374, 389
  offset: 1, 1
  index: -1
Gelo seco
  rotate: false
  xy: 2, 1096
  size: 1874, 934
  orig: 1876, 936
  offset: 1, 1
  index: -1
Olho
  rotate: false
  xy: 2, 135
  size: 914, 959
  orig: 916, 961
  offset: 1, 1
  index: -1
Pupila
  rotate: true
  xy: 2594, 1010
  size: 305, 104
  orig: 307, 106
  offset: 1, 1
  index: -1
Reflexo
  rotate: false
  xy: 1878, 1317
  size: 880, 713
  orig: 882, 715
  offset: 1, 1
  index: -1
