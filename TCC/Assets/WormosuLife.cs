﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Spine.Unity;

public class WormosuLife : MonoBehaviour {

	public Wormosu wormosu;
	public float life = 4;
	public GameObject peso;

	private bool canGo;
	private bool startedDie;

	public float shakeAmt;
	public float timeShaking;
	private bool isShaking;

	public GameObject explosion;
	public GameObject elevator;

	public GameObject explosionPlayer;
	public GameObject player;

	public GameObject part2;
	public GameObject part3;
	public GameObject part4;

	public Color color1;
	public Color color2;
	public Color color3;
	public Color color4;

	private bool playerDied;

	public GameObject bodyBoggy;

	void Start () {
		StartCoroutine (CooldownToActive());
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "PoisonEnemy" && !wormosu.goingUp && this.enabled && canGo && other.GetComponent<EnemyThorn>().canGo) {
			if (life > 0) {
				life--;
				SoundController.PlaySoundBoss (SoundBossType.BossEffect3);
				Damage ();

			}
			else {
				InvokeRepeating ("CameraShake", 0, 0.01f);
				if (!startedDie) {
					GetComponentInChildren<SkeletonAnimation> ().timeScale = 0;
					StartCoroutine (StartToDestroy ());
					startedDie = true;
					Camera.main.GetComponent<CameraControllerV3> ().target = this.gameObject.transform;
					Camera.main.GetComponent<CameraControllerV3> ().enabled = false;
					SoundController.PlaySoundBoss (SoundBossType.BossEffect4);
					GameObject.Destroy (peso);
				}
			}
		}

		if (other.tag == "Player" || other.tag == "NoPlayer") {
			if (!playerDied) {
				StartCoroutine (GameOver ());
				playerDied = true;
			}

		}

		if (Input.GetKey (KeyCode.F1) && player.GetComponent<Cheat>().enabled) {
			life = -1;
		}
	}

	IEnumerator CooldownToActive() {
		yield return new WaitForSeconds (2);
		canGo = true;
	}

	void Damage() {
		if (life == 3) {
			part2.GetComponent<SpriteRenderer> ().color = color1;
			part3.GetComponent<SpriteRenderer> ().color = color1;
			part4.GetComponent<SpriteRenderer> ().color = color1;
			GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (1, "Estagio 1", true);
		}
		else if (life == 2) {
			part2.GetComponent<SpriteRenderer> ().color = color2;
			part3.GetComponent<SpriteRenderer> ().color = color2;
			part4.GetComponent<SpriteRenderer> ().color = color2;
			GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (1, "Estagio 2", true);
		}
		else if (life == 1) {
			part2.GetComponent<SpriteRenderer> ().color = color3;
			part3.GetComponent<SpriteRenderer> ().color = color3;
			part4.GetComponent<SpriteRenderer> ().color = color3;
			GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (1, "Estagio 3", true);
		}
		else if (life == 0) {
			part2.GetComponent<SpriteRenderer> ().color = color4;
			part3.GetComponent<SpriteRenderer> ().color = color4;
			part4.GetComponent<SpriteRenderer> ().color = color4;
			GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (1, "Estagio 4", true);
		}

	}

	IEnumerator StartToDestroy() {
		float timer = 0;
		bool right = true;

		yield return new WaitForSeconds (.5f);
//		CancelInvoke ("CameraShake");

//		while(timer < 3)
//		{
//			if (right) {
//				this.transform.position = new Vector2 (transform.position.x + .1f, this.transform.position.y);
//				right = false;
//			} 
//			else {
//				this.transform.position = new Vector2 (transform.position.x - .1f, this.transform.position.y);
//				right = true;
//			}
//				
//			timer += Time.fixedDeltaTime;
//			yield return new WaitForFixedUpdate();
//		}
		SoundController.PlayBGM (SoundTypeBGM.World1);
		Camera.main.GetComponent<CameraControllerV3> ().target = wormosu.playerPos;
		Camera.main.GetComponent<CameraControllerV3> ().enabled = true;
		explosion.transform.position = transform.position;
		explosion.SetActive (true);
		GameObject.Destroy (this.gameObject);
		GameObject.Destroy (part2);
		GameObject.Destroy (part3);
		GameObject.Destroy (part4);
		elevator.GetComponent<Elevator> ().enabled = true;
	}

	IEnumerator GameOver() {
		explosionPlayer.transform.position = player.transform.position;
		Death death = player.GetComponent<Death>();
		death.KillCharacter (DeathType.Deafult);
		GameObject.Destroy (player);
		yield return new WaitForSeconds (.5f);
		SoundController.PlaySound (SoundType.BossDeath);
		Camera.main.GetComponent<CameraControllerV3> ().target = bodyBoggy.transform;
		explosionPlayer.SetActive (true);
		yield return new WaitForSeconds (5);
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}

	void CameraShake() {
		float quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;
		Vector3 pp = Camera.main.transform.position;
		pp.y += quakeAmt;
		pp.x += quakeAmt;
		Camera.main.transform.position = pp;
	}
}
