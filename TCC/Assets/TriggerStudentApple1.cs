﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class TriggerStudentApple1 : MonoBehaviour {

	public GameObject student;
	public string studentPath;
	public float studentTime;
	public string initialAnim;
	private bool arrived;

	void Update () {
		if (student.GetComponentInChildren<SkeletonAnimation>().state.ToString () == initialAnim && !arrived) {
			iTween.MoveTo (student, iTween.Hash ("path", iTweenPath.GetPath (studentPath), "time", studentTime, "easetype", iTween.EaseType.linear));
			arrived = true;
		}
		if (student.GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent (0) == null && arrived) {
			student.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Preso Loop", true);
			GameObject.Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			this.enabled = true;
		}
	}
}
