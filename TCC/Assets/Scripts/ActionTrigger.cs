﻿using UnityEngine;
using System.Collections;

public class ActionTrigger : MonoBehaviour {

	public string searchForTag;
	public DeathType death;
	public GameObject[] troco;
	private int trocoNb = 0;

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == searchForTag) {
			if (death == DeathType.Slide) {
				if (other.GetComponent<CollisionState> ().standing) {
					other.GetComponent<Slide> ().enabled = true;
				}
			} 
			else if (death == DeathType.Fart) {
				if (!other.GetComponent<CollisionState> ().standing && other.transform.position.y > transform.GetComponent<BoxCollider2D> ().bounds.max.y) {
					other.GetComponent<Fart> ().direction = transform.localScale.x;
					other.GetComponent<Fart> ().enabled = true;
					if (troco != null) {
						troco[trocoNb].transform.position = other.transform.position;
						float randomDir = Random.Range (0, 10);
						float random = 0;
						if (randomDir <= 5) {
							random = Random.Range (10, 15);
						} else {
							random = Random.Range (-15, -10);
						}
						troco [trocoNb].GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
						troco[trocoNb].GetComponent<Rigidbody2D> ().AddForce (new Vector2 (1 * random, 1 * random), ForceMode2D.Impulse);
						troco[trocoNb].GetComponent<Rigidbody2D> ().AddTorque (1000);
						trocoNb++;
						if (trocoNb > 5) {
							trocoNb = 0;
						}
					}
				}
			} 
			else if (death == DeathType.Stick) {
				SoundController.PlaySound (SoundType.Stick);
			} 
		}
	}

	void OnTriggerStay2D (Collider2D other) {
		if (other.tag == searchForTag) {
			if (death == DeathType.Stick) {
				if (other.GetComponent<CollisionState> ().onWall) {
					 // Change to Stick

					if (transform.localScale.x == -1 && other.GetComponent<InputState> ().direction == Directions.Right) {
						other.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
                        other.GetComponent<WallJump>().enabled = true;
                    }
                    else if (transform.localScale.x == 1 && other.GetComponent<InputState> ().direction == Directions.Left) {
						other.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
                        other.GetComponent<WallJump>().enabled = true;
                    }
                    else
                    {
                        other.GetComponent<WallJump>().enabled = false;
                    }
				} else {
					other.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeRotation;
				}
			}
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == searchForTag) {
			if (death == DeathType.Stick) {
				other.GetComponent<WallJump> ().enabled = false;
				other.GetComponent<Jump> ().jumpsRemaining = 1;
			}
		}
	}
}
