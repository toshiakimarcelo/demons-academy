﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class EnemyLaser : MonoBehaviour {

//	public GameObject laserPrefab;
	public GameObject[] lasers;
	public float cdLaser;

	private Transform player;

	private SpriteRenderer sprite;

	private bool canGo = true;

	public bool triggered;

	// Use this for initialization
	void Awake() {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		//sprite = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 dir = player.position - transform.position;
		float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

		if (GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent(0) == null) GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation(0, "MamiloIdle", true);
		if (GetComponentInParent<SkeletonAnimation> ().state.GetCurrent(0) == null) GetComponentInParent<SkeletonAnimation> ().state.SetAnimation(0, "InimigoCongeladorIdle", true);

		if (canGo) {
			Invoke ("CooldownLaser", cdLaser);
			canGo = false;
		}
	}
	
	void CooldownLaser() {
		if (triggered) {
			Invoke ("EnemyLaserGo", .75f);
			if (GetComponentInParent<SkeletonAnimation> ().state.ToString () != "InimigoCongeladorAtaque") GetComponentInParent<SkeletonAnimation> ().state.SetAnimation (0, "InimigoCongeladorAtaque", false);
			if (GetComponentInChildren<SkeletonAnimation> ().state.ToString() != "MamiloAtaque") GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation(0, "MamiloAtaque", false);
			triggered = false;
		} 
		else {
			canGo = true;
		}
	}

	void EnemyLaserGo() {
		for (int i = 0; i < lasers.Length; i++) {
			if (lasers [i].GetComponent<Laser> ().canGo) {
				
				lasers [i].transform.position = transform.position + transform.right;
				lasers [i].transform.rotation = transform.rotation;
				lasers [i].GetComponent<Laser> ().StartLaser ();
				lasers [i].GetComponent<Laser> ().canGo = false;
				break;
			}
		}
		SoundController.PlaySoundEnemy (SoundEnemyType.EnemyPlatform, transform.position);
		canGo = true;
	}
}
