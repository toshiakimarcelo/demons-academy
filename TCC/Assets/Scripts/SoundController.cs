﻿using UnityEngine;
using System.Collections;

public enum SoundType {Jump, DoubleJump, Checkpoint, Spawn, SlideDeath, FartDeath, StickDeath, ImmunityDeath, PlatformDeath, FallDeath, Slide, Fart, Stick, EndGame, StartGame, Win, BossDeath}; 
public enum SoundEnemyType {EnemyThorn, EnemyShooter, EnemyPoison, EnemyImmunity, EnemyPlatform};
public enum SoundStudentType {Jump1, Jump2, Fall, Fly, TreeDeath, SlideDeath, StickDeath, PlatformDeath, JumpDeath, Wormosu1, Wormosu2};
public enum SoundBossType {BossEffect1, BossEffect2, BossEffect3, BossEffect4, BossEffect5, BossEffect6, BossEffect7, BossEffect8, Tits1, Tits2, Tits3}
public enum SoundTypeBGM {Menu, World1, World2, BossWormosu1, BossWormosu2, BossAudumbla1, BossAudumbla2}
public enum SoundTypeMenu {Go, Back}

public class SoundController : MonoBehaviour {

	public static SoundController soundController;
	public static AudioSource audioSourceMenu;
	public static AudioSource audioSourceBGM;
	public static AudioSource audioSourceBoggy;
	public static AudioSource audioSourceCheckpoint;
	public static AudioSource audioSourceEnemyThorn;
	public static AudioSource audioSourceEnemyShooter;
	public static AudioSource audioSourceEnemyPoison;
	public static AudioSource audioSourceEnemyImmunity;
	public static AudioSource audioSourceEnemyPlatform;
	public static AudioSource audioSourceBoss;
	public static AudioSource audioSourceBossDeath;
	public static AudioSource audioSourceBossTits1;
	public static AudioSource audioSourceBossTits2;
	public static AudioSource audioSourceBossTits3;
	public static AudioSource audioSourceStudent1;
	public static AudioSource audioSourceStudent2;
	public static AudioSource audioSourceStudent3;
	public static AudioSource audioSourceStudent4;
	public static AudioSource audioSourceStudent5;
	public static AudioSource audioSourceTutorial;

	public GameObject audioSourceMenuGobject;
	public GameObject audioSourceBGMGobject;
	public GameObject audioSourceBoggyGobject;
	public GameObject audioSourceCheckpointGobject;
	public GameObject audioSourceEnemyThornGobject;
	public GameObject audioSourceEnemyShooterGobject;
	public GameObject audioSourceEnemyPoisonGobject;
	public GameObject audioSourceEnemyImmunityGobject;
	public GameObject audioSourceEnemyPlatformGobject;
	public GameObject audioSourceBossGobject;
	public GameObject audioSourceBossDeathGobject;
	public GameObject audioSourceBossTits1Gobject;
	public GameObject audioSourceBossTits2Gobject;
	public GameObject audioSourceBossTits3Gobject;
	public GameObject audioSourceStudentGobject1;
	public GameObject audioSourceStudentGobject2;
	public GameObject audioSourceStudentGobject3;
	public GameObject audioSourceStudentGobject4;
	public GameObject audioSourceStudentGobject5;
	public GameObject audioSourceTutorialGobject;

	public AudioClip go;
	public AudioClip back;
	public AudioClip Menu;
	public AudioClip BGMWorld1;
	public AudioClip BGMWorld2;
	public AudioClip BGMBossWormosu1;
	public AudioClip BGMBossWormosu2;
	public AudioClip BGMBossAudumbla1;
	public AudioClip BGMBossAudumbla2;
	public AudioClip jump;
	public AudioClip doubleJump;
	public AudioClip checkpoint;
	public AudioClip spawn;
	public AudioClip slideDeath;
	public AudioClip fartDeath;
	public AudioClip stickDeath;
	public AudioClip immunityDeath;
	public AudioClip platformDeath;
	public AudioClip fallDeath;
	public AudioClip slide;
	public AudioClip fart;
	public AudioClip stick;
	public AudioClip endGameSound;
	public AudioClip startGameSound;
	public AudioClip win;
	public AudioClip bossDeath;
	public AudioClip enemyThorn;
	public AudioClip enemyShooter;
	public AudioClip enemyPoison;
	public AudioClip enemyImmunity;
	public AudioClip enemyPlatform;
	public AudioClip bossWormosu1;
	public AudioClip bossWormosu2;
	public AudioClip bossWormosu3;
	public AudioClip bossWormosu4;
	public AudioClip bossAudumbla1;
	public AudioClip bossAudumbla2;
	public AudioClip bossAudumbla3;
	public AudioClip[] bossAudumbla4;
	public AudioClip tits1;
	public AudioClip tits2;
	public AudioClip tits3;
	public AudioClip[] dialogue;
	public AudioClip[] dialogueThyrone;
	public AudioClip student1Jump;
	public AudioClip student2Jump;
	public AudioClip studentFall;
	public AudioClip studentFly;
	public AudioClip studentTreeDeath;
	public AudioClip studentSlideDeath;
	public AudioClip studentStickDeath;
	public AudioClip studentPlatformDeath;
	public AudioClip studentJumpDeath;
	public AudioClip studentWormosu1;
	public AudioClip studentWormosu2;

	public BoxCollider2D cameraLimitSound;

	private bool check;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this.gameObject);
		DontDestroyOnLoad (cameraLimitSound.gameObject);
		cameraLimitSound.size = new Vector2 (Camera.main.orthographicSize*4*Camera.main.aspect,Camera.main.orthographicSize*4);
		audioSourceMenu = audioSourceMenuGobject.GetComponent<AudioSource> ();
		audioSourceBGM = audioSourceBGMGobject.GetComponent<AudioSource> ();
		audioSourceBoggy = audioSourceBoggyGobject.GetComponent<AudioSource> ();
		audioSourceCheckpoint = audioSourceCheckpointGobject.GetComponent<AudioSource> ();
		audioSourceEnemyThorn = audioSourceEnemyThornGobject.GetComponent<AudioSource> ();
		audioSourceEnemyShooter = audioSourceEnemyShooterGobject.GetComponent<AudioSource> ();
		audioSourceEnemyPoison = audioSourceEnemyPoisonGobject.GetComponent<AudioSource> ();
		audioSourceEnemyImmunity = audioSourceEnemyImmunityGobject.GetComponent<AudioSource> ();
		audioSourceEnemyPlatform = audioSourceEnemyPlatformGobject.GetComponent<AudioSource> ();
		audioSourceBoss = audioSourceBossGobject.GetComponent<AudioSource> ();
		audioSourceBossDeath = audioSourceBossDeathGobject.GetComponent<AudioSource> ();
		audioSourceBossTits1 = audioSourceBossTits1Gobject.GetComponent<AudioSource> ();
		audioSourceBossTits2 = audioSourceBossTits2Gobject.GetComponent<AudioSource> ();
		audioSourceBossTits3 = audioSourceBossTits3Gobject.GetComponent<AudioSource> ();
		audioSourceStudent1 = audioSourceStudentGobject1.GetComponent<AudioSource> ();
		audioSourceStudent2 = audioSourceStudentGobject2.GetComponent<AudioSource> ();
		audioSourceStudent3 = audioSourceStudentGobject3.GetComponent<AudioSource> ();
		audioSourceStudent4 = audioSourceStudentGobject4.GetComponent<AudioSource> ();
		audioSourceStudent5 = audioSourceStudentGobject5.GetComponent<AudioSource> ();

		audioSourceTutorial = audioSourceTutorialGobject.GetComponent<AudioSource> ();
		soundController = GetComponent<SoundController> ();
	}

	void FixedUpdate() {
		cameraLimitSound.transform.position = Camera.main.transform.position;
	}

	public static void PlayMenuSFX(SoundTypeMenu soundType) {
		if (soundType == SoundTypeMenu.Go) {
			audioSourceMenu.clip = soundController.go;
			audioSourceMenu.Play ();
		} 
		else if (soundType == SoundTypeMenu.Back) {
			audioSourceMenu.clip = soundController.back;
			audioSourceMenu.Play ();
		}
	}

	public static void PlaySound(SoundType soundType){
		if (soundType == SoundType.Jump) {
			audioSourceBoggy.clip = soundController.jump;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.05f;
		} 
		else if (soundType == SoundType.DoubleJump) {
			audioSourceBoggy.clip = soundController.doubleJump;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.05f;
		} 
		else if (soundType == SoundType.Checkpoint) {
			audioSourceCheckpoint.clip = soundController.checkpoint;
			audioSourceCheckpoint.Play ();
			audioSourceCheckpoint.volume = 0.2f;
		}
		else if (soundType == SoundType.Spawn) {
			audioSourceBoggy.clip = soundController.spawn;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.SlideDeath) {
			audioSourceBoggy.clip = soundController.slideDeath;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.FartDeath) {
			audioSourceBoggy.clip = soundController.fartDeath;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.StickDeath) {
			audioSourceBoggy.clip = soundController.stickDeath;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.ImmunityDeath) {
			audioSourceBoggy.clip = soundController.immunityDeath;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.PlatformDeath) {
			audioSourceBoggy.clip = soundController.platformDeath;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.FallDeath) {
			audioSourceBoggy.clip = soundController.fallDeath;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.Slide) {
			audioSourceBoggy.clip = soundController.slide;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.Fart) {
			audioSourceBoggy.clip = soundController.fart;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.Stick) {
			audioSourceBoggy.clip = soundController.stick;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.2f;
		}
		else if (soundType == SoundType.EndGame) {
			audioSourceBoggy.clip = soundController.endGameSound;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.1f;
		}
		else if (soundType == SoundType.StartGame) {
			audioSourceBoggy.clip = soundController.startGameSound;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.1f;
		}
		else if (soundType == SoundType.Win) {
			audioSourceBoggy.clip = soundController.win;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.1f;
		}
		else if (soundType == SoundType.BossDeath) {
			audioSourceBoggy.clip = soundController.bossDeath;
			audioSourceBoggy.Play ();
			audioSourceBoggy.volume = 0.1f;
		}

	}

	public static void PlaySoundEnemy(SoundEnemyType soundType, Vector2 position){
		if ((position.x <= soundController.cameraLimitSound.bounds.max.x && position.x >= soundController.cameraLimitSound.bounds.min.x) && 
			(position.y <= soundController.cameraLimitSound.bounds.max.y && position.y >= soundController.cameraLimitSound.bounds.min.y) ) {
			if (soundType == SoundEnemyType.EnemyThorn) {
				audioSourceEnemyThorn.clip = soundController.enemyThorn;
				audioSourceEnemyThorn.Play ();
			}
			else if (soundType == SoundEnemyType.EnemyShooter) {
				audioSourceEnemyShooter.clip = soundController.enemyShooter;
				audioSourceEnemyShooter.Play ();
			}
			else if (soundType == SoundEnemyType.EnemyPoison) {
				audioSourceEnemyPoison.clip = soundController.enemyPoison;
				audioSourceEnemyPoison.Play ();
			}
			else if (soundType == SoundEnemyType.EnemyImmunity) {
				audioSourceEnemyImmunity.clip = soundController.enemyImmunity;
				audioSourceEnemyImmunity.Play ();
			}
			else if (soundType == SoundEnemyType.EnemyPlatform) {
				audioSourceEnemyPlatform.clip = soundController.enemyPlatform;
				audioSourceEnemyPlatform.Play ();
			}
		}

	}

	public static void PlaySoundStudent(SoundStudentType soundType, Vector2 position){
		if ((position.x <= soundController.cameraLimitSound.bounds.max.x && position.x >= soundController.cameraLimitSound.bounds.min.x) &&
		    (position.y <= soundController.cameraLimitSound.bounds.max.y && position.y >= soundController.cameraLimitSound.bounds.min.y)) {
			if (soundType == SoundStudentType.Jump1) {
				audioSourceStudent1.clip = soundController.student1Jump;
				audioSourceStudent1.Play ();
			}
			else if (soundType == SoundStudentType.Jump2) {
				audioSourceStudent2.clip = soundController.student2Jump;
				audioSourceStudent2.Play ();
			}
			else if (soundType == SoundStudentType.Fall) {
				audioSourceStudent2.clip = soundController.studentFall;
				audioSourceStudent2.Play ();
			}
			else if (soundType == SoundStudentType.Fly) {
				audioSourceStudent5.clip = soundController.studentFly;
				audioSourceStudent5.Play ();
			}
			else if (soundType == SoundStudentType.TreeDeath) {
				audioSourceStudent1.clip = soundController.studentTreeDeath;
				audioSourceStudent1.Play ();
			}
			else if (soundType == SoundStudentType.Wormosu1) {
				audioSourceStudent2.clip = soundController.studentWormosu1;
				audioSourceStudent2.Play ();
			}
			else if (soundType == SoundStudentType.Wormosu2) {
				audioSourceStudent2.clip = soundController.studentWormosu2;
				audioSourceStudent2.Play ();
			}
			else if (soundType == SoundStudentType.SlideDeath) {
				audioSourceStudent1.clip = soundController.studentSlideDeath;
				audioSourceStudent1.Play ();
			}
			else if (soundType == SoundStudentType.StickDeath) {
				audioSourceStudent5.clip = soundController.studentStickDeath;
				audioSourceStudent5.Play ();
			}
			else if (soundType == SoundStudentType.PlatformDeath) {
				audioSourceStudent4.clip = soundController.studentPlatformDeath;
				audioSourceStudent4.Play ();
			}
			else if (soundType == SoundStudentType.JumpDeath) {
				audioSourceStudent4.clip = soundController.studentJumpDeath;
				audioSourceStudent4.Play ();
			}
		}
	}

	public static void PlaySoundBoss(SoundBossType soundType) {
		if (soundType == SoundBossType.BossEffect1) {
			audioSourceBoss.clip = soundController.bossWormosu1;
			audioSourceBoss.Play ();
		}
		else if (soundType == SoundBossType.BossEffect2) {
			audioSourceBoss.clip = soundController.bossWormosu2;
			audioSourceBoss.Play ();
		}
		else if (soundType == SoundBossType.BossEffect3) {
			audioSourceBoss.clip = soundController.bossWormosu3;
			audioSourceBoss.Play ();
		}
		else if (soundType == SoundBossType.BossEffect4) {
			audioSourceBoss.clip = soundController.bossWormosu4;
			audioSourceBoss.Play ();
		}
		else if (soundType == SoundBossType.BossEffect5) {
			audioSourceBoss.clip = soundController.bossAudumbla1;
			audioSourceBoss.Play ();
		}
		else if (soundType == SoundBossType.BossEffect6) {
			audioSourceBoss.clip = soundController.bossAudumbla2;
			audioSourceBoss.Play ();
		}
		else if (soundType == SoundBossType.BossEffect7) {
			audioSourceBossDeath.clip = soundController.bossAudumbla3;
			audioSourceBossDeath.Play ();
		}
		else if (soundType == SoundBossType.BossEffect8) {
			int i = Random.Range (0, 4);
			audioSourceBoss.clip = soundController.bossAudumbla4 [i];
			audioSourceBoss.Play ();
		}
		else if (soundType == SoundBossType.Tits1) {
			audioSourceBossTits1.clip = soundController.tits1;
			audioSourceBossTits1.Play ();
		}
		else if (soundType == SoundBossType.Tits2){
			audioSourceBossTits2.clip = soundController.tits2;
			audioSourceBossTits2.Play ();
		}
		else if (soundType == SoundBossType.Tits3) {
			audioSourceBossTits3.clip = soundController.tits3;
			audioSourceBossTits3.Play ();
		}
	}

	public static void PlaySoundDialogue() {
		int i = Random.Range (0, 5);
		audioSourceTutorial.clip = soundController.dialogue [i];
		audioSourceTutorial.Play ();
	}

	public static void PlaySoundDialogueThyrone() {
		int i = Random.Range (0, 5);
		audioSourceTutorial.clip = soundController.dialogueThyrone [i];
		audioSourceTutorial.Play ();
	}

	public static void PlayBGM(SoundTypeBGM soundType) {
		if (soundType == SoundTypeBGM.Menu && audioSourceBGM.clip != soundController.Menu) {
			audioSourceBGM.clip = soundController.Menu;
			audioSourceBGM.loop = true;
			audioSourceBGM.Play ();
			audioSourceBGM.volume = 0.1f;
		} else if (soundType == SoundTypeBGM.World1 && audioSourceBGM.clip != soundController.BGMWorld1) {
			audioSourceBGM.clip = soundController.BGMWorld1;
			audioSourceBGM.loop = true;
			audioSourceBGM.Play ();
			audioSourceBGM.volume = 0.1f;
		} else if (soundType == SoundTypeBGM.World2 && audioSourceBGM.clip != soundController.BGMWorld2) {
			audioSourceBGM.clip = soundController.BGMWorld2;
			audioSourceBGM.loop = true;
			audioSourceBGM.Play ();
			audioSourceBGM.volume = 0.1f;
		} else if (soundType == SoundTypeBGM.BossWormosu1 && audioSourceBGM.clip != soundController.BGMBossWormosu1) {
			audioSourceBGM.clip = soundController.BGMBossWormosu1;
			audioSourceBGM.loop = false;
			audioSourceBGM.Play ();
			audioSourceBGM.volume = 0.6f;
		} else if (soundType == SoundTypeBGM.BossWormosu2 && audioSourceBGM.clip != soundController.BGMBossWormosu2) {
			audioSourceBGM.clip = soundController.BGMBossWormosu2;
			audioSourceBGM.loop = true;
			audioSourceBGM.Play ();
			audioSourceBGM.volume = 0.6f;
		}
		else if (soundType == SoundTypeBGM.BossAudumbla1 && audioSourceBGM.clip != soundController.BGMBossAudumbla1) {
			audioSourceBGM.clip = soundController.BGMBossAudumbla1;
			audioSourceBGM.loop = false;
			audioSourceBGM.Play ();
			audioSourceBGM.volume = 0.4f;
		}
		else if (soundType == SoundTypeBGM.BossAudumbla2 && audioSourceBGM.clip != soundController.BGMBossAudumbla2) {
			audioSourceBGM.clip = soundController.BGMBossAudumbla2;
			audioSourceBGM.loop = true;
			audioSourceBGM.Play ();
			audioSourceBGM.volume = 0.4f;
		}

	}

	public static void StopBGM() {
		audioSourceBGM.Stop ();
	}
//	public void Play(AudioClip audio) {
//		 (audio);
//	}

	void OnLevelWasLoaded(int level) {
		if (level >= 2)
			check = true;
		if (level == 1 && check)
		Destroy (this.gameObject);
	}
}
