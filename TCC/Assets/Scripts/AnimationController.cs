﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class AnimationController : MonoBehaviour {

	[HideInInspector] public SkeletonAnimation skeletonAnimation;
	private CollisionState collisionState;
	private InputState inputState;
	private Rigidbody2D body2d;
	private Jump jump;
	private Walk walk;
	private WallJump wallJump;
	private VelocityXController velocityX;

	public MeshRenderer Boggy1;
	public MeshRenderer Boggy2;
	public MeshRenderer Boggy3;

	[HideInInspector] public bool win;
	private bool started;
	private bool canReadOnEnable;

	private bool initialStick;

	void OnEnable() {
		if (canReadOnEnable) {
			if (skeletonAnimation.state.ToString () != "Spawn2") skeletonAnimation.state.SetAnimation (0, "Spawn2", false);
			GetComponentInParent<Walk> ().enabled = false;
			GetComponentInParent<Jump> ().enabled = false;
			GetComponentInParent<FaceDirection> ().enabled = false;
			started = true;
			SoundController.PlaySound (SoundType.Spawn);
		}
	}

	void OnDisable() {
		Boggy1.enabled = false;
		//skeletonAnimation.state.SetAnimation (0, "Spawn2", false);
	}

	void Awake() {
		skeletonAnimation = GetComponent<SkeletonAnimation> ();
		collisionState = GetComponentInParent<CollisionState>();
		inputState = GetComponentInParent<InputState>();
		body2d = GetComponentInParent<Rigidbody2D> ();
		jump = GetComponentInParent<Jump> ();
		walk = GetComponentInParent<Walk> ();
		wallJump = GetComponentInParent<WallJump> ();
		velocityX = GetComponentInParent<VelocityXController> ();
	}

	void Start () {
		skeletonAnimation.state.Complete += delegate(Spine.AnimationState state, int trackIndex, int loopCount) {
//			if (skeletonAnimation.state.ToString () == "Pouso") ChangeAnimationState("Idle");
		};

		skeletonAnimation.state.Event += delegate(Spine.AnimationState state, int trackIndex, Spine.Event e) {
//			if (skeletonAnimation.state.ToString () == "Pouso") ChangeAnimationState("Idle");
		};

		Boggy1.enabled = true;
		GetComponentInParent<Walk> ().enabled = false;
		GetComponentInParent<Jump> ().enabled = false;
		GetComponentInParent<FaceDirection> ().enabled = false;
		started = true;
		canReadOnEnable = true;
	}

	void Update () {
		if (skeletonAnimation.state.ToString () != "Spawn2") {
			
			if (started) {
				GetComponentInParent<Walk> ().enabled = true;
				GetComponentInParent<Jump> ().enabled = true;
				GetComponentInParent<FaceDirection> ().enabled = true;
				started = false;
			}

			if (inputState.absVelX < walk.maxVelocity) {
				Boggy1.enabled = true;
				Boggy2.enabled = false;
			}

			if (!wallJump.enabled && Boggy3.enabled) {
				Boggy1.enabled = true;
				Boggy3.enabled = false;
				initialStick = false;
			}

			if (GetComponentInParent<Death>().immune) {
				if (skeletonAnimation.state.ToString () != "Idle Imune")
					ChangeAnimationState ("Idle Imune");
			}
			else if (win) {
				if (skeletonAnimation.state.ToString () != "Vitoria Bom Loop") ChangeAnimationState ("Vitoria Bom Loop");
			}
			else if (body2d.tag == "Cheat") {
				if (skeletonAnimation.state.ToString () != "Cheatsy Doodle")
					ChangeAnimationState ("Cheatsy Doodle");
			} 
			else if (body2d.velocity.y > 0 && jump.jumpsRemaining == 1) {
				if (skeletonAnimation.state.ToString () != "Pulo")
					ChangeAnimationState ("Pulo");
				if (Boggy3.enabled) {
					Boggy1.enabled = true;
					Boggy3.enabled = false;
					initialStick = false;
				}
			} 
			else if (body2d.velocity.y > 0 && jump.jumpsRemaining == 0) {
				if (skeletonAnimation.state.ToString () != "Pulo Duplo")
					ChangeAnimationState ("Pulo Duplo");
				if (Boggy2.enabled) {
					Boggy1.enabled = true;
					Boggy2.enabled = false;
				}
			} 
			else if (body2d.velocity.y < -.1f) {
				if (skeletonAnimation.state.ToString () != "Pulo Queda")
					ChangeAnimationState ("Pulo Queda");
				if (Boggy3.enabled) {
					Boggy1.enabled = true;
					Boggy3.enabled = false;
					initialStick = false;
				}
			} 
			else if (velocityX.breakVelocityValue < velocityX.originalVelocityValue && inputState.absVelX > 0) {
				if (skeletonAnimation.state.ToString () != "Boggy Escorrega")
					ChangeAnimationState ("Boggy Escorrega");
			} 
			else if (wallJump.enabled && collisionState.onWall) {
				Boggy1.enabled = false;
				Boggy3.enabled = true;

				if (!initialStick) {
					Boggy3.GetComponent<SkeletonAnimation> ().state.SetAnimation (0, "EfeitoGrudeInicio", false);
					initialStick = true;
				} else if (Boggy3.GetComponent<SkeletonAnimation> ().state.GetCurrent (0) == null)
					Boggy3.GetComponent<SkeletonAnimation> ().state.SetAnimation (0, "EfeitoGrudeIdle", true);

			} 
			else if (inputState.absVelX > walk.maxVelocity + 1f && !Boggy2.enabled && walk.maxVelocity >= 5) {
				Boggy1.enabled = false;
				Boggy2.enabled = true;
				Boggy2.GetComponent<SkeletonAnimation> ().state.SetAnimation (0, "EfeitoDeslize", true);
			} 
			else if (inputState.absVelX > 0) {
				if (skeletonAnimation.state.ToString () == "Pulo Queda") {
					skeletonAnimation.state.SetAnimation (0, "Walk", true);
					//  skeletonAnimation.state.SetAnimation(0, "Pouso", false);
				} else if (skeletonAnimation.state.ToString () != "Walk")
					ChangeAnimationState ("Walk");
			} 
			else if (collisionState.standing) {
				if (skeletonAnimation.state.ToString () == "Pulo Queda")
					skeletonAnimation.state.SetAnimation (0, "Pouso", false);
				else if (skeletonAnimation.state.ToString () == "Pouso")
					skeletonAnimation.state.AddAnimation (0, "Idle", true, 0f);
				else if (skeletonAnimation.state.ToString () != "Idle")
					ChangeAnimationState ("Idle");
			}
		} 
		else if (skeletonAnimation.state.ToString () == "Spawn2") {
			Boggy1.enabled = true;
			Boggy2.enabled = false;
			Boggy3.enabled = false;
		}
	}

	public void ChangeAnimationState(string animation){
		skeletonAnimation.state.SetAnimation(0, animation, true);
	}
}

//private InputState     inputState;
//private Walk           walkBehavior;
//private Animator       animator;
//private CollisionState collisionState;
//private Duck           duckBehavior;
//
//public void Awake() {
//	inputState     = GetComponent<InputState>();
//	walkBehavior   = GetComponent<Walk>();
//	animator       = GetComponent<Animator>();
//	collisionState = GetComponent<CollisionState>();
//	duckBehavior   = GetComponent<Duck>();
//}
//
//public void Update() {
//	
//
//	if (inputState.absVelX > 0) {
//		ChangeAnimationState(1);
//	}
//
//	if (inputState.absVelY > 0) {
//		ChangeAnimationState(2);
//	}
//
//	animator.speed = walkBehavior.running ? walkBehavior.runMultiplier : 1;
//
//	if (duckBehavior.ducking) {
//		ChangeAnimationState(3);
//	}
//
//	if (!collisionState.standing && collisionState.onWall) {
//		ChangeAnimationState(4);
//	}
//}


