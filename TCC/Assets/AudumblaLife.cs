﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class AudumblaLife : MonoBehaviour {

	public float value;
	public GameObject pinto;
	public GameObject sound;
	public GameObject elevator;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (value == 4) {
			GetComponent<SkeletonAnimation> ().state.SetAnimation (0, "AudumblaMorte", false);
			GameObject.Destroy (pinto);
			GameObject.Destroy (sound);
			elevator.GetComponent<Elevator> ().enabled = true;
			SoundController.PlaySoundBoss (SoundBossType.BossEffect7);
			SoundController.PlayBGM (SoundTypeBGM.World2);

			this.enabled = false;
		}
	}
}
