﻿using UnityEngine;
using System.Collections;

public class Quicksand : MonoBehaviour {

	public string tagPlayer;

	private float originalJumpForce;
	private float originalWalkSpeed;
	private float originalMaxVelocity;

	// Use this for initialization
	void Start () {
	
	}

	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == tagPlayer || other.tag == "NoPlayer") {
			originalJumpForce = other.GetComponent<Jump> ().jumpForce;
			other.GetComponent<Jump> ().jumpForce = 90;

			originalWalkSpeed = other.GetComponent<Walk> ().speed;
			other.GetComponent<Walk> ().speed = 1;
			originalMaxVelocity = other.GetComponent<Walk> ().maxVelocity;
			other.GetComponent<Walk> ().maxVelocity = 1;
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == tagPlayer || other.tag == "NoPlayer") {
			other.GetComponent<Jump> ().jumpForce = originalJumpForce;

			other.GetComponent<Walk> ().speed = originalWalkSpeed;
			other.GetComponent<Walk> ().maxVelocity = originalMaxVelocity;
		}
	}
}
