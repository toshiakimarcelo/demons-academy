﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class StudentAnimController : MonoBehaviour {

	public string name;
	public string anim;
	public bool loop;
	public bool sound;
	public SoundStudentType soundType;
	public float rotation;
	public bool concha;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
			
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.name == name && anim != "") {
			other.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, anim, loop ? true : false);
		}
		if (other.name == name && concha) {
			other.transform.eulerAngles = new Vector3(0,0,rotation);
		}
		if (other.name == name && sound) {
			SoundController.PlaySoundStudent (soundType,transform.position);
		}
	}
}
