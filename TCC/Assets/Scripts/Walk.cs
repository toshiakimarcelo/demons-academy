﻿using UnityEngine;
using System.Collections;

public class Walk : AbstractBehavior {
	public float speed = 5f;
	public float maxVelocity = 5f;
	bool right;
	bool left;

	[HideInInspector] public float originalSpeed;
	[HideInInspector] public float originalMaxVelocity;

	void Start() {
		originalSpeed = speed;
		originalMaxVelocity = maxVelocity;
	}

	public void Update() {
		right = inputState.GetButtonValue (inputButtons [0]);
		left = inputState.GetButtonValue (inputButtons [1]);
	}


	public void FixedUpdate() {
		if ((right || left) && !collisionState.onWall) {
			float tmpSpeed = speed;

			float velX = tmpSpeed * (float)inputState.direction;

			if (((float)inputState.direction > 0 && body2d.velocity.x >= 0) || 
				((float)inputState.direction < 0 && body2d.velocity.x <= 0)) {
				body2d.AddForce (new Vector2 (velX, 0));
			}
			else if (((float)inputState.direction < 0 && body2d.velocity.x > 0) ||
				((float)inputState.direction > 0 && body2d.velocity.x < 0)) {
				velocityX.BreakVelocity ();
			}
			if (GetComponent<Rigidbody2D> ().constraints != RigidbodyConstraints2D.FreezeRotation) GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeRotation;
			velocityX.CalculateMaxVelocity (maxVelocity);
		} 
		else {
			velocityX.BreakVelocity ();
		}
	}
}
