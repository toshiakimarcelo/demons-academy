﻿using UnityEngine;
using System.Collections;

public class Respawn : AbstractBehavior {

	public int nbRespawn;
	public float cdRespawn;
	private bool canRespawn = true;
	//private int actualNbRespawn;

	private Transform desiredPosition;

	void Start() {
		//actualNbRespawn = nbRespawn;
	}

	void Update () {
		bool respawnButton = inputState.GetButtonValue (inputButtons [0]);
		float respawnButtonTime = inputState.GetButtonHoldTime (inputButtons [0]);

		if (respawnButton && respawnButtonTime < 0.1f && canRespawn) {
			Invoke ("Cooldown", cdRespawn);
			canRespawn = false;
			SetRespawnLocation ();
		}
	}

	void SetRespawnLocation () {
		//GameObject respawnPoint = GameObject.FindGameObjectWithTag ("Respawn");
		//GameObject character = GameObject.FindGameObjectWithTag ("Player");
		//desiredPosition = character.transform;

		//if (desiredPosition.transform.position != respawnPoint.transform.position) {
			//actualNbRespawn--;
			//CheckpointCount.MinusCheckpoint (actualNbRespawn);
			//respawnPoint.transform.position = desiredPosition.transform.position;
			//SoundController.PlaySound (SoundType.Checkpoint);
		//}
		if (!collisionState.checkpoint && collisionState.standing) {
			GameObject respawnPoint = GameObject.FindGameObjectWithTag ("Respawn");
			GameObject character = GameObject.FindGameObjectWithTag ("Player");
            desiredPosition = character.transform;
			GradeCheckpoints.Checkpoint ();

            if (desiredPosition.transform.position != respawnPoint.transform.position) {
//				actualNbRespawn--;
//				CheckpointCount.MinusCheckpoint (actualNbRespawn);
				respawnPoint.transform.position = desiredPosition.transform.position;
			    SoundController.PlaySound (SoundType.Checkpoint);
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Checkpoint") {
			desiredPosition = other.transform;
		}
	}

	void Cooldown() {
		canRespawn = true;
	}
}
