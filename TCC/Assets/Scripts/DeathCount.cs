﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeathCount : MonoBehaviour {

	public GameObject parent;

	private static DeathCount deathCount;
	public Text text;

	public int count;

	void Start() {
		count = PlayerPrefs.GetInt ("death");
		DontDestroyOnLoad (this.gameObject);
		DontDestroyOnLoad (parent.gameObject);
		deathCount = GetComponent<DeathCount> ();
		deathCount.text.text = PlayerPrefs.GetInt ("death").ToString();
	}

	public static void PlusDeath () {
		deathCount.count++;
		deathCount.text.text = deathCount.count.ToString ();
		//PlayerPrefs.SetInt ("death", deathCount.count);
	}
}
