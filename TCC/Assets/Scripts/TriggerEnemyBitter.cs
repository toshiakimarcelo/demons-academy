﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class TriggerEnemyBitter : MonoBehaviour {

	public string tagPlayer;

	private GameObject player;
	public bool chase;

	public GameObject enemyBitter;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (chase) {
			enemyBitter.GetComponent<EnemyBitter> ().enabled = false;
			enemyBitter.transform.position = Vector2.MoveTowards (enemyBitter.transform.position, player.transform.position,enemyBitter.GetComponent<EnemyBitter>().speedChase*Time.deltaTime) ;
			enemyBitter.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
			if (GetComponentInChildren<SkeletonAnimation> ().state.ToString () != "Investida") GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Investida", true);
			if (enemyBitter.transform.position.x >= player.transform.position.x) {
				GetComponentInChildren<SkeletonAnimation> ().transform.localScale = new Vector3 (-1,1,1);
			} 
			else {
				GetComponentInChildren<SkeletonAnimation> ().transform.localScale = new Vector3 (1,1,1);
			}
		}
		else {
			enemyBitter.GetComponent<EnemyBitter> ().enabled = true;
			enemyBitter.GetComponent<EnemyBitter> ().startAgain = true;
			if (GetComponentInChildren<SkeletonAnimation> () != null) GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation(0, "Idle", true);
			this.enabled = false;
		}

		if (player != null && !player.activeSelf) {
			chase = false;
		}
	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == tagPlayer) {
			player = other.gameObject;
			chase = true;
			this.enabled = true;
			SoundController.PlaySoundEnemy (SoundEnemyType.EnemyImmunity, transform.position);
		} 
	}
	void OnTriggerExit2D(Collider2D other){
		if (other.tag == tagPlayer) {
			chase = false;
		}
	}
}
