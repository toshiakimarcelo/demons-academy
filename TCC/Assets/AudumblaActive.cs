﻿using UnityEngine;
using System.Collections;

public class AudumblaActive : MonoBehaviour {

	public GameObject[] tetas;
	public GameObject boggy;

	void Start () {
	
	}

	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			boggy.GetComponent<Respawn> ().enabled = true;
			SoundController.PlaySoundBoss (SoundBossType.BossEffect5);
			SoundController.PlayBGM (SoundTypeBGM.BossAudumbla1);
			GameObject.Destroy (this.gameObject);
			for (int i = 0; i < tetas.Length; i++) {
				tetas [i].SetActive (true);
			}
		}
	}
}
