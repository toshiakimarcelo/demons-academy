﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class TriggerStudentGoblin1 : MonoBehaviour {

	public GameObject student;
	public string initialAnim;
	public GameObject laser;

	void Update () {
		if (student.GetComponentInChildren<SkeletonAnimation>().state.ToString () == initialAnim) {
			laser.GetComponent<Laser> ().StartLaser ();
			laser.GetComponent<Laser> ().canGo = false;
			GameObject.Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			this.enabled = true;
		}
	}
}
