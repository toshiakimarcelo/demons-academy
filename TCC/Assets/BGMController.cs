﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BGMController : MonoBehaviour {

	private float level;
	private bool started;

	// Use this for initialization
	void Start () {
		started = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!SoundController.audioSourceBGM.isPlaying) {
			if (SceneManager.GetActiveScene ().buildIndex == 17) {
				SoundController.PlayBGM (SoundTypeBGM.BossWormosu2);
			} 
			else if (SceneManager.GetActiveScene ().buildIndex == 33) {
				SoundController.PlayBGM (SoundTypeBGM.BossAudumbla2);
			}
		}
		if (started) {
			if (SceneManager.GetActiveScene ().buildIndex < 17) {
				SoundController.PlayBGM (SoundTypeBGM.World1);
			} 
			if (SceneManager.GetActiveScene ().buildIndex == 17) {
				SoundController.PlayBGM (SoundTypeBGM.World1);
			} 
			else if (SceneManager.GetActiveScene ().buildIndex == 33) {
				SoundController.PlayBGM (SoundTypeBGM.World2);
			}
			else if (SceneManager.GetActiveScene ().buildIndex > 17 && SceneManager.GetActiveScene ().buildIndex < 33) {
				SoundController.PlayBGM (SoundTypeBGM.World2);
			}
			else if (SceneManager.GetActiveScene ().buildIndex == 34) {
				SoundController.PlayBGM (SoundTypeBGM.Menu);
			}

			started = false;
		}
	}
}
