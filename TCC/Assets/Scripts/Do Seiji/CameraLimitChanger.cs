﻿using UnityEngine;
using System.Collections;

public class CameraLimitChanger : MonoBehaviour
{
	public string playerTag;
	public int camLimitNumber;
	public CameraControllerV3 camScipt;

	void Start ()
	{
		camScipt = Camera.main.gameObject.GetComponent<CameraControllerV3>();
	}

	void OnTriggerEnter2D (Collider2D c)
	{
		if (c.tag == playerTag)
		{
			camScipt.potato = 0;
			camScipt.change = true;
			camScipt.index = camLimitNumber;
		}
	}
}
