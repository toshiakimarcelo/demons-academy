﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckLevelEnable : MonoBehaviour {

	public Button button;
	public Text text;
	public Text text2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (button.interactable) {
			text.enabled = true;
			text2.enabled = true;
		}
	}
}
