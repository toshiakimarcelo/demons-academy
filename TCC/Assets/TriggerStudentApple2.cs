﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class TriggerStudentApple2 : MonoBehaviour {

	public GameObject student;
	public string initialAnim;
	public SoundStudentType soundType;

	void Update () {
		if (student.GetComponentInChildren<SkeletonAnimation>().state.ToString () == initialAnim) {
			student.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
			student.GetComponentInChildren<SkeletonAnimation>().state.SetAnimation (0, "Morte", true);
			SoundController.PlaySoundStudent (soundType,transform.position);
			GameObject.Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			this.enabled = true;
		}
	}
}
