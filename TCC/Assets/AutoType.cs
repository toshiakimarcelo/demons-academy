﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using Spine.Unity;

public class AutoType : MonoBehaviour {

	public float letterPause = 0.2f;

	public Text text;
	public Image image;

	public float inBetweenTextPause = 1f;

	public string[] message;

	public AutoType aTMain;
	public GameObject triggerStart;
	public GameObject triggerStop;

	public GameObject professor;
	private MeshRenderer pMeshRend;
	private SkeletonAnimation pSkelAnim;
	private Vector3 pOGScale;

	private GameObject player;
	private bool isTalking;
	private int i;

	private GameObject HUD;

	private int count;
	public GameObject peixe;

	void Start ()
	{
//		i = 0;
//		Invoke ("Cooldown", 1f);
		HUD = GameObject.Find("HUD");
		if (SceneManager.GetActiveScene ().buildIndex == 35) HUD.SetActive (false);
		if (SceneManager.GetActiveScene ().buildIndex == 34) HUD.SetActive (false);
		if (professor != null)
		{
			pMeshRend = professor.GetComponent<MeshRenderer>();
			pSkelAnim = professor.GetComponent<SkeletonAnimation>();

			pMeshRend.enabled = false;
			pOGScale = professor.transform.localScale;

			player = GameObject.FindGameObjectWithTag("Player");
		}

//		for (int i = 0; i < triggerStop.Length; i++)
//		{
//			if (triggerStop[i].GetComponent<BoxCollider2D>().enabled) triggerStop[i].GetComponent<BoxCollider2D>().enabled = false;
//		}
	}

	void Update (){
		if (player != null)
		{
			if (player.transform.position.x < professor.transform.position.x)
				professor.transform.localScale = new Vector3 (pOGScale.x, pOGScale.y, pOGScale.z);
			else if (player.transform.position.x > professor.transform.position.x)
				professor.transform.localScale = new Vector3 (pOGScale.x * -1, pOGScale.y, pOGScale.z);
		}
	}

	private void StartTyping() {
		StartCoroutine ("TypeText");
	}

	private void StopTyping()
	{
		text.text = "";
		CancelInvoke();
		StopAllCoroutines ();
		image.enabled = false;
		text.text = "";
	}

	private IEnumerator TypeText ()
	{
		if (professor != null) {
			image.enabled = true;
			if (pSkelAnim.state.ToString() == "Idle")
				pSkelAnim.state.SetAnimation(0, "Falar", true);
		}

		if (peixe != null) {
			image.enabled = true;
		}

		if (i < message.Length) {
			foreach (char letter in message[i].ToCharArray()) {
				SoundController.PlaySoundDialogueThyrone ();
				count++;

				text.text += letter;
				yield return new WaitForSeconds (letterPause);
			}
		} 

		if (count >= 4) {
			
			count = 0;
		}

		yield return new WaitForSeconds (letterPause);

		if (professor != null) {
			if (pSkelAnim.state.ToString() == "Falar")
				pSkelAnim.state.SetAnimation(0, "Idle", true);
		}
		
		yield return new WaitForSeconds (inBetweenTextPause);

		if (i < message.Length) {
			i++;
			text.text = "";
			if (professor != null){
				image.enabled = false;
			}
			if (peixe != null) {
				image.enabled = false;
			}
			StartTyping();
		} 
		else {
			StopCoroutine ("TypeText");
			if (professor != null){
				image.enabled = false;
			}
			if (peixe != null) {
				image.enabled = false;
			}
		}

		yield return null;
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (this.gameObject == triggerStart && c.tag == "Player" && !aTMain.isTalking)
		{
			aTMain.i = 0;
			aTMain.Invoke ("StartTyping", 2f);

			aTMain.isTalking = true;

			if (pMeshRend != null) {
				pMeshRend.enabled = true;
				pSkelAnim.state.SetAnimation (0, "Surgir", false);
				if (pSkelAnim.state.ToString () == "Surgir")
					pSkelAnim.state.AddAnimation (0, "Idle", true, 0);
			}
		}
		if (this.gameObject == triggerStop && c.tag == "Player" && aTMain.isTalking)
		{
			aTMain.StopTyping();
			aTMain.isTalking = false;

			if (pMeshRend != null) pSkelAnim.state.SetAnimation(0, "Sumir", false);
		}
	}


	private void AnimationEventLoadLevel(int level)
	{
		if(HUD != null) HUD.SetActive (true);
		SceneManager.LoadScene (level);
	}
}