﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum DeathType {Slide, Fart, Stick, Platform, Immunity, Deafult}; 

public class Death : AbstractBehavior {

	public int maxNbResources;

	public GameObject deathGravityPrefab;
	public GameObject slidePrefab;
	public GameObject fartPrefab;
	public GameObject stickPrefab;
	public GameObject platformPrefab;
	public GameObject immunityPrefab;
	public GameObject defaultPrefab;

	public float timeToRespawn;

	private Transform characterPosition;
	private Transform respawnPoint;

	private CameraControllerV3 camController;
	private InputState inputState;

	private List<GameObject> slideDeaths = new List<GameObject>();
	private List<GameObject> fartDeaths = new List<GameObject>();
	private List<GameObject> stickDeaths = new List<GameObject>();
	private List<GameObject> platformDeaths = new List<GameObject>();
	private List<GameObject> immunityDeaths = new List<GameObject>();

	[HideInInspector] public bool immune;
	private bool firstEnable = true;

	void Awake() {
		camController = Camera.main.GetComponent<CameraControllerV3> ();
		inputState = GetComponent<InputState> ();
	}

	void OnEnable () {
		characterPosition = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform>();
		respawnPoint = GameObject.FindGameObjectWithTag ("Respawn").GetComponent<Transform>();

        //GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

        //GetComponent<Walk>().enabled = true;
		//GetComponent<Jump>().enabled = true;
		GetComponent<Jump>().jumping = false;
		GetComponent<Jump>().jumpsRemaining = GetComponent<Jump>().jumpCount;
		GetComponent<Fart>().enabled = false;
		GetComponent<Slide>().enabled = false;
		GetComponent<WallJump>().enabled = false;
		if(GetComponent<VelocityXController>().originalVelocityValue!= 0) GetComponent<VelocityXController>().breakVelocityValue = GetComponent<VelocityXController>().originalVelocityValue;
//		if(GetComponent<Jump>().originalJumpForce != 0) GetComponent<Jump>().jumpForce = GetComponent<Jump>().originalJumpForce;
		if(GetComponent<Jump>().originalJumpForce != 0) GetComponent<Jump>().jumpForce = 115;
		if(GetComponent<Jump>().jumpCount != 2) GetComponent<Jump>().jumpCount = 2;
		if(GetComponent<Walk>().originalSpeed != 0) GetComponent<Walk>().speed = GetComponent<Walk>().originalSpeed;
//		if(GetComponent<Walk>().originalMaxVelocity != 0) GetComponent<Walk>().maxVelocity = GetComponent<Walk>().originalMaxVelocity;
		if(GetComponent<Walk>().originalMaxVelocity != 0) GetComponent<Walk>().maxVelocity = 6.7f;
		inputState.absVelX = 0;
		inputState.absVelY = 0;
		if (firstEnable) {
			firstEnable = false;
		}
		else {
			immune = true;
			GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
		}
	}

	void OnDisable () {
		Invoke ("RespawnCharacter", timeToRespawn);
		tag = "Player";
	}

	void Update() {
		if ((inputState.absVelX != 0 || inputState.absVelY != 0) && immune) {
			immune = false;
			tag = "Player";
			GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeRotation;
		}
		if (immune && tag != "NoPlayer") {
			tag = "NoPlayer";
		}
	}

	public void KillCharacter(DeathType deathType) {
		//DeathCount.PlusDeath ();

		gameObject.SetActive (false);

		Transform gravity = Instantiate (deathGravityPrefab).transform;

		Vector3 scaleDesired = new Vector3 (characterPosition.transform.localScale.x, 1, 1);
		Vector3 positionDesired = characterPosition.transform.position;

		gravity.transform.position = positionDesired;

		Transform deathTarget = gravity;

		GameObject deathResource = null;

		if (deathType == DeathType.Slide) {
			deathResource = slidePrefab; //Morte Slide
			SoundController.PlaySound (SoundType.SlideDeath);
		} else if (deathType == DeathType.Fart) {
			deathResource = fartPrefab;
			SoundController.PlaySound (SoundType.FartDeath);
		} else if (deathType == DeathType.Stick) {
			deathResource = stickPrefab;
			SoundController.PlaySound (SoundType.StickDeath);
		} else if (deathType == DeathType.Platform) {
			deathResource = platformPrefab;
			scaleDesired = new Vector3 (characterPosition.transform.localScale.x*platformPrefab.transform.localScale.x, platformPrefab.transform.localScale.y, platformPrefab.transform.localScale.z);
			//SoundController.PlaySound (SoundType.StickDeath);
		}
		else if (deathType == DeathType.Immunity) {
			deathResource = immunityPrefab;
			scaleDesired = new Vector3 (characterPosition.transform.localScale.x*immunityPrefab.transform.localScale.x, immunityPrefab.transform.localScale.y, immunityPrefab.transform.localScale.z);
			//SoundController.PlaySound (SoundType.StickDeath);
		}
		else deathResource = defaultPrefab;

		deathResource.transform.localScale = scaleDesired;
		deathResource.transform.position = positionDesired;

		Instantiate (deathResource).transform.parent = gravity;

		CountDeaths (gravity.gameObject, deathType);

		camController.target = deathTarget;
	}

	void RespawnCharacter() {
		gameObject.SetActive (true);

		characterPosition.position = respawnPoint.position;

		camController.target = characterPosition;
	}

	void CountDeaths(GameObject deathResource, DeathType deathType) {
		if (deathType == DeathType.Slide) {
			slideDeaths.Add (deathResource);

			if (slideDeaths.Count > maxNbResources) {
				Destroy (slideDeaths [0]);

				slideDeaths.RemoveAt (0);
			}
		} else if (deathType == DeathType.Fart) {
			fartDeaths.Add (deathResource);

			if (fartDeaths.Count > maxNbResources) {
				Destroy(fartDeaths [0]);

				fartDeaths.RemoveAt (0);
			}
		} else if (deathType == DeathType.Stick) {
			stickDeaths.Add (deathResource);

			if (stickDeaths.Count > maxNbResources) {
				Destroy(stickDeaths [0]);

				stickDeaths.RemoveAt (0);
			}
		} else if (deathType == DeathType.Platform) {
			platformDeaths.Add (deathResource);

			if (platformDeaths.Count > maxNbResources) {
				Destroy(platformDeaths [0]);

				platformDeaths.RemoveAt (0);
			}
		}
		else if (deathType == DeathType.Immunity) {
			immunityDeaths.Add (deathResource);

			if (immunityDeaths.Count > maxNbResources) {
				Destroy(immunityDeaths [0]);

				immunityDeaths.RemoveAt (0);
			}
		}
	}
}
