﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {

	public GameObject endGameObject;
	public GameObject grade;

	public GameObject image;
	public string tagPlayer;

	public GameObject player;
	public float velocity;

	[HideInInspector] public bool resetLevel;

	[HideInInspector] public bool canGo;

	public bool firstTime = true;

	private bool arrived;

	private GameObject placaFinalVoadora;
	private MeshRenderer pfvMeshRen;
	private Animator pfvAnim;

	//public GameObject tutorial;

	private bool endGameSound;
	private bool startGameSound;

	// Use this for initialization
	void Start () {
		if (SceneManager.GetActiveScene ().buildIndex > PlayerPrefs.GetInt ("level")) 
		PlayerPrefs.SetInt ("level", SceneManager.GetActiveScene ().buildIndex -1);

		placaFinalVoadora = GameObject.Find("PlacaFinalVoadora");
		pfvMeshRen = placaFinalVoadora.GetComponent<MeshRenderer>();
		pfvAnim = placaFinalVoadora.GetComponent<Animator>();
		pfvMeshRen.enabled = pfvAnim.enabled = false;
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == tagPlayer) {
			pfvMeshRen.enabled = pfvAnim.enabled = true;
			endGameObject.SetActive (true);
			grade.SetActive (true);
			arrived = true;
			player.GetComponent<Walk> ().enabled = false;
			player.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, player.GetComponent<Rigidbody2D> ().velocity.y);
			player.GetComponent<Jump> ().enabled = false;
			player.GetComponent<FaceDirection> ().enabled = false;
			player.transform.localScale = new Vector2 (1, 1);
			player.GetComponentInChildren<AnimationController> ().win = true;
			player.tag = "NoPlayer";
			player.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
			SoundController.PlaySound (SoundType.Win);
			player.GetComponent<Respawn> ().enabled = false;
		}
	}



	void Update() {
		if (arrived) {
			if (Input.GetButtonDown ("Fire1")) {
				canGo = true;
			}

			if (Input.GetButtonDown ("Fire2")) {
				SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
			}
		}
	}

	void FixedUpdate() {
		if (canGo) {
			if (!endGameSound) {
				endGameSound = true;
				SoundController.PlaySound (SoundType.EndGame);
			}

			player.GetComponent<Walk> ().enabled = false;
			player.GetComponent<Jump> ().enabled = false;
			player.GetComponent<FaceDirection> ().enabled = false;
			image.GetComponent<SpriteRenderer> ().enabled = true;
			image.transform.localScale += new Vector3 (velocity*Camera.main.pixelWidth*Time.deltaTime*0.01f, velocity*Camera.main.pixelHeight*Time.deltaTime*0.01f,0);

			if (image.transform.localScale.x >= 40) {
				if (resetLevel) {
					SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
					SoundController.PlayBGM (SoundTypeBGM.World1);
				} 
				else {
					SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
				}

			}
		}


		if (firstTime) {
			if (!startGameSound) {
				startGameSound = true;
				SoundController.PlaySound (SoundType.StartGame);
			}

			player.GetComponent<Walk> ().enabled = false;
			player.GetComponent<Jump> ().enabled = false;
			player.GetComponent<FaceDirection> ().enabled = false;
			image.GetComponent<SpriteRenderer> ().enabled = true;
			image.transform.localScale -= new Vector3 (velocity*Camera.main.pixelWidth*Time.deltaTime*0.01f, velocity*Camera.main.pixelHeight*Time.deltaTime*0.01f,0);

			if (image.transform.localScale.x <= 0) {
				firstTime = false;
				player.GetComponent<Walk> ().enabled = true;
				player.GetComponent<Jump> ().enabled = true;
				player.GetComponent<FaceDirection> ().enabled = true;
				image.GetComponent<SpriteRenderer> ().enabled = false;
			}
		}

	}
}
