﻿using UnityEngine;
using System.Collections;

public class BossWormosu : MonoBehaviour {

	public GameObject cameraLimit;
	public GameObject player;
	public GameObject boss;
	public GameObject endGame;

	public string tagPlayer;

	public float speed;

	private BoxCollider2D box;
	private float distanceBossCameraLimit;

	public float shakeAmt;
	public float timeShaking;
	private bool isShaking;

	private bool startedBoss;

	void Awake() {
		distanceBossCameraLimit = cameraLimit.transform.position.x - boss.transform.position.x;
		boss.transform.position = new Vector2(0,boss.transform.position.y);
	}

	void FixedUpdate() {
		if (isShaking) {
			//SoundController.StopBGM ();

			Camera.main.GetComponent<CameraControllerV3> ().enabled = false;
			player.GetComponent<Walk> ().enabled = false;
			player.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, player.GetComponent<Rigidbody2D> ().velocity.y);
			player.GetComponent<Jump> ().enabled = false;
			player.GetComponent<FaceDirection> ().enabled = false;
			player.transform.localScale = new Vector2 (-1, 1);

			InvokeRepeating ("CameraShake", 0, 0.01f);
			if (!startedBoss) {
				SoundController.PlaySoundBoss (SoundBossType.BossEffect1);
				SoundController.PlayBGM (SoundTypeBGM.BossWormosu1);

				Invoke ("StopShaking", timeShaking);
				startedBoss = true;
			}

			GetComponent<BoxCollider2D> ().enabled = false;
		} 
		else if (!isShaking) {

			if (!SoundController.audioSourceBGM.isPlaying) {
				SoundController.PlayBGM (SoundTypeBGM.BossWormosu2);
			}

			if (!SoundController.audioSourceBoss.isPlaying) {
				if (Random.Range (0, 10) >= 95) {
					SoundController.PlaySoundBoss (SoundBossType.BossEffect2);
				} 
				else {
					SoundController.PlaySoundBoss (SoundBossType.BossEffect3);
				}

			}

			if (cameraLimit.transform.position.x - boss.transform.position.x <= distanceBossCameraLimit) {
				cameraLimit.transform.position += new Vector3 (speed, 0, 0) * Time.deltaTime;
			} 
			else {
				boss.transform.position += new Vector3 (speed, 0, 0) * Time.deltaTime * 40;
			}

			if (player.transform.position.x < boss.GetComponent<BoxCollider2D> ().bounds.max.x) {
				endGame.GetComponent<NextLevel> ().resetLevel = true;
				endGame.GetComponent<NextLevel> ().canGo = true;
			}
		}


	}

	void CameraShake() {
		float quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;
		Vector3 pp = Camera.main.transform.position;
		pp.y += quakeAmt;
		pp.x += quakeAmt;
		Camera.main.transform.position = pp;
	}

	void StopShaking() {
		SoundController.PlaySoundBoss (SoundBossType.BossEffect2);
		isShaking = false;
		Camera.main.GetComponent<CameraControllerV3> ().enabled = true;
		player.GetComponent<Walk> ().enabled = true;
		player.GetComponent<Jump> ().enabled = true;
		player.GetComponent<FaceDirection> ().enabled = true;
		CancelInvoke ("CameraShake");

		//CancelInvoke ("StopShaking");


	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == tagPlayer) {
			GetComponent<BossWormosu>().enabled = true;
			isShaking = true;
		}
	}


}
