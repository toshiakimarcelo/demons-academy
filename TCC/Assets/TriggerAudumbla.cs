﻿using UnityEngine;
using System.Collections;

public class TriggerAudumbla : MonoBehaviour {

	public Audumbla audumbla;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerStay2D (Collider2D other) {
		if (other.tag == "Player") {
			audumbla.triggered = true;
		} 
		else if (other.gameObject == null) {
			audumbla.triggered = false;
		}
	}
}
