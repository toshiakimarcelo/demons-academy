﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	public GameObject worlds;
	public AudioMixer audioMixer;

	public GameObject mainMenu;
	public GameObject worldSelect;
	public GameObject worldPanel1;
	public GameObject worldPanel2;
	public GameObject options;
	public GameObject credits;
	public GameObject instructions;

	public GameObject world2Bt;

	public GameObject pause;
	public GameObject buttonPause;

	public GameObject[] levels;
	public Image[] gradeImage;

	private bool isRight;

	public GameObject canvas;

	private Text res;

	public Text worldInfo;

//	public GameObject 

	// Use this for initialization
	void Start () {
		//PlayerPrefs.DeleteAll ();
		if (!PlayerPrefs.HasKey ("res")) {
			PlayerPrefs.SetInt ("res", 1);
			PlayerPrefs.SetInt ("resWidth", Screen.width); 
			PlayerPrefs.SetInt ("resHeight", Screen.height); 
		}
		if (PlayerPrefs.HasKey ("masterVol")) {
			audioMixer.SetFloat ("master", PlayerPrefs.GetFloat ("masterVol"));
			audioMixer.SetFloat ("bgm", PlayerPrefs.GetFloat ("bgmVol"));
			audioMixer.SetFloat ("sfx", PlayerPrefs.GetFloat ("sfxVol"));
		}
		if (!PlayerPrefs.HasKey("level")) PlayerPrefs.SetInt ("level", 1);
		if (!PlayerPrefs.HasKey("FirstTimePlaying")) PlayerPrefs.SetString ("FirstTimePlaying", "true");

		Invoke ("Read", 1);

//		if (PlayerPrefs.GetInt ("level") > 17) {
//			if(world2Bt != null) world2Bt.GetComponent<Button> ().interactable = true;
//		}
	}

	public void ChangeScreen(GameObject screen) {
		if (screen == worldSelect && PlayerPrefs.GetString("FirstTimePlaying") == "true") {
			PlayerPrefs.SetString("FirstTimePlaying", "false");
			GetComponent<StartLevelTEMP>().LoadIntroScene(35);

		}
//		if (screen != GameObject.Find("World Select Menu V2")) 
		else {
			SoundController.PlayMenuSFX (SoundTypeMenu.Go);
			screen.SetActive (true);

		}
	}

	public void CloseScreen(GameObject oldScreen) {
		oldScreen.SetActive (false);
	}

	public void ButtonStart(GameObject button) {
		button.GetComponent<Selectable> ().Select ();
	}

	public void Read() {
		if (SceneManager.GetActiveScene ().buildIndex == 1) {
			for (int i = 0; i < levels.Length; i++) {
				if (i < PlayerPrefs.GetInt ("level")) {
					levels [i].GetComponent<Button> ().interactable = true;
					string level = (i + 1).ToString ();
					if (PlayerPrefs.HasKey ("GradeValue" + level)) {
						gradeImage [i].enabled = true;
						levels [i].GetComponentInChildren<Text> ().text = PlayerPrefs.GetString ("GradeText" + level);
					}
				}
			}
		}
		if (SceneManager.GetActiveScene ().buildIndex == 1) {
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = true;
		}
		if (PlayerPrefs.GetInt ("level") > 17) {
			if(world2Bt != null) world2Bt.GetComponent<Button> ().interactable = true;
		}
	}

	public void ReadErase() {
		for (int i = 0; i < levels.Length; i++) {
			levels [i].GetComponent<Button> ().interactable = false;
		}
	}

	void Update() {
		
//			if (worldScreen.activeSelf) {
//				if (Input.GetKeyDown(KeyCode.RightArrow)) {
//					SoundController.PlayMenuSFX (SoundTypeMenu.Go);
//					worldNb++;
//					isRight = true;
//					if (worldNb > 2)
//						worldNb = 2;
//				}
//				else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
//					SoundController.PlayMenuSFX (SoundTypeMenu.Go);
//					worldNb--;
//					isRight = false;
//					if (worldNb < 0)
//						worldNb = 0;
//				}
//			}
//				
//			pos2 = Vector3.Distance (dist1.transform.position, dist2.transform.position);
//			pos3 = -Vector3.Distance (dist1.transform.position, dist2.transform.position)/2;
		//}




		if (Input.GetKeyDown (KeyCode.Escape) && SceneManager.GetActiveScene ().buildIndex == 1) {
			
			if (options.activeSelf) {
				SoundController.PlayMenuSFX (SoundTypeMenu.Back);
				mainMenu.SetActive (true);
				options.SetActive (false);
			} 
			else if (credits.activeSelf) {
				SoundController.PlayMenuSFX (SoundTypeMenu.Back);
				mainMenu.SetActive (true);
				credits.SetActive (false);
			}
			else if (instructions.activeSelf) {
				SoundController.PlayMenuSFX (SoundTypeMenu.Back);
				mainMenu.SetActive (true);
				instructions.SetActive (false);
			}
			else if (worldSelect.activeSelf) {
				SoundController.PlayMenuSFX (SoundTypeMenu.Back);
				mainMenu.SetActive (true);
				worldSelect.SetActive (false);
			}
			else if (worldPanel1.activeSelf) {
				SoundController.PlayMenuSFX (SoundTypeMenu.Back);
				worldSelect.SetActive (true);
				worldPanel1.SetActive (false);
			}
			else if (worldPanel2.activeSelf) {
				SoundController.PlayMenuSFX (SoundTypeMenu.Back);
				worldSelect.SetActive (true);
				worldPanel2.SetActive (false);
			}
		} else if (Input.GetKeyDown (KeyCode.Escape) && SceneManager.GetActiveScene ().buildIndex >= 2) {
			
			if (!pause.activeSelf && !options.activeSelf) {
				SoundController.PlayMenuSFX (SoundTypeMenu.Back);
				pause.SetActive (true);
				Time.timeScale = 0;
				Cursor.lockState = CursorLockMode.Confined;
				Cursor.visible = true;
			}
			else if (pause.activeSelf && !options.activeSelf) {
				SoundController.PlayMenuSFX (SoundTypeMenu.Back);
				pause.SetActive (false);
				Time.timeScale = 1;
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
			else if (!pause.activeSelf && options.activeSelf) {
				FinishOption();
				pause.SetActive(true);
			}
		}


	}

	public void FinishOption() {
		if (SceneManager.GetActiveScene ().buildIndex == 1 && options.activeSelf) {
			SoundController.PlayMenuSFX (SoundTypeMenu.Go);
			mainMenu.SetActive (true);
			options.SetActive (false);
		} else {
			options.SetActive (false);
			SoundController.PlayMenuSFX (SoundTypeMenu.Back);
			//Time.timeScale = 0;
		}
	}
		
	public void SetMasterVol (float vol)
	{
		if (vol <= -40)
			vol = -80;
		audioMixer.SetFloat("master", vol);
		PlayerPrefs.SetFloat ("masterVol", vol);
	}

	public void SetBGMVol (float vol)
	{
		if (vol <= -40)
			vol = -80;
		audioMixer.SetFloat("bgm", vol);
		PlayerPrefs.SetFloat ("bgmVol", vol);
	}

	public void SetSFXVol (float vol)
	{
		if (vol <= -40)
			vol = -80;
		audioMixer.SetFloat("sfx", vol);
		PlayerPrefs.SetFloat ("sfxVol", vol);
	}

	public void SetDialogueVol (float vol)
	{
		if (vol <= -40)
			vol = -80;
		audioMixer.SetFloat("dialogue", vol);
		PlayerPrefs.SetFloat ("dialogueVol", vol);
	}

	public void FullScreen (bool fullScreen)
	{
		if (fullScreen) {
			PlayerPrefs.SetInt ("res", 1);
			Screen.SetResolution (PlayerPrefs.GetInt("resWidth"), PlayerPrefs.GetInt("resHeight"), true);

		} 
		else {
			PlayerPrefs.SetInt ("res", 0);
			Screen.SetResolution (PlayerPrefs.GetInt("resWidth"), PlayerPrefs.GetInt("resHeight"), false);
		}
	}

	public void UpdateWorldInfo(string info) {
		worldInfo.text = info;
	}

	public void ResetWorldInfo() {
		worldInfo.text = "";
	}

	public void Unpause() {
		SoundController.PlayMenuSFX (SoundTypeMenu.Go);
		pause.SetActive (false);
		Time.timeScale = 1;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	public void Restart() {
		SoundController.PlayMenuSFX (SoundTypeMenu.Go);
		pause.SetActive (false);
		Time.timeScale = 1;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void Options() {
		SoundController.PlayMenuSFX (SoundTypeMenu.Go);
		options.SetActive (true);
		pause.SetActive (false);
		Time.timeScale = 0;
	}

	public void Home() {
		SoundController.PlayMenuSFX (SoundTypeMenu.Go);
		SceneManager.LoadScene (1);
		Time.timeScale = 1;
		Destroy (canvas);
	}

	public void Quit() {
		Application.Quit ();
	}
}
