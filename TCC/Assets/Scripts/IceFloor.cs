﻿using UnityEngine;
using System.Collections;

public class IceFloor : MonoBehaviour {

	public string tagPlayer;


	private float originalBreakVelocity;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == tagPlayer || other.tag == "NoPlayer") {
			originalBreakVelocity = other.GetComponent<VelocityXController> ().breakVelocityValue;
			other.GetComponent<VelocityXController> ().breakVelocityValue = 5;
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == tagPlayer || other.tag == "NoPlayer") {
			other.GetComponent<VelocityXController> ().breakVelocityValue = originalBreakVelocity;
		}
	}
}
