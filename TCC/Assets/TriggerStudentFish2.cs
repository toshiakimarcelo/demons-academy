﻿using UnityEngine;
using System.Collections;

public class TriggerStudentFish2 : MonoBehaviour {

	public GameObject student;
	public GameObject explosion;


	void OnTriggerEnter2D(Collider2D other){
		if (other.name == student.name) {
			explosion.transform.position = other.transform.position;
			explosion.SetActive (true);

			GameObject.Destroy (other.gameObject);
		}
	}
}
