﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using UnityEngine.SceneManagement;

public class FinalScene : MonoBehaviour {

	public GameObject thyrone;
	private GameObject boggy;

	void Update() {
		if (thyrone != null) {
			if (thyrone.GetComponentInChildren<SkeletonAnimation> ().state.GetCurrent (0) == null) {
				thyrone.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Idle", true);
				boggy.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Idle costas com diploma", true);
			}
		}
	}


	void OnTriggerEnter2D(Collider2D other){
		boggy = other.gameObject;
		other.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Idle", true);
		Invoke ("EntregaDiploma", 1f);
	}

	void OnTriggerExit2D(Collider2D other){
		other.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Walk Diploma", true);
	}

	void EntregaDiploma() {
		boggy.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Idle costas", true);
		thyrone.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Diploma", false);
	}

	public void Menu() {
		SceneManager.LoadScene (1);
	}
}
