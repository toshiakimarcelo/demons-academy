﻿using UnityEngine;
using System.Collections;

public class AudumblaSound : MonoBehaviour {

	public bool go;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!go) {
			go = true;
			StartCoroutine (Idle ());
		}
	}

	IEnumerator Idle() {
		yield return new WaitForSeconds (5);
		SoundController.PlaySoundBoss (SoundBossType.BossEffect8);
		go = false;
	}
}
