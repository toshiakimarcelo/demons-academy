﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GradeCheckpoints : MonoBehaviour {

	private static GradeCheckpoints gradeCheckpoints;

	public float checkpoint;

	public Text text;

	public Text textHud;

	// Use this for initialization
	void Start () {
		gradeCheckpoints = GetComponent<GradeCheckpoints> ();
		if (GameObject.Find ("GradeCheckpointHUD")) textHud = GameObject.Find ("GradeCheckpointHUD").GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		text.text = checkpoint.ToString ();
		if (textHud != null) textHud.text = checkpoint.ToString ();
	}
	public static void Checkpoint () {
		gradeCheckpoints.checkpoint++;
	}
}
