﻿using UnityEngine;
using System.Collections;

public class TriggerStudentsLevel1 : MonoBehaviour {


	public GameObject[] students;
	public string[] studentsPath;
	public float[] studentsTime;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			for (int i = 0; i < students.Length; i++) {
				iTween.MoveTo (students[i], iTween.Hash ("path", iTweenPath.GetPath (studentsPath[i]), "time", studentsTime[i], "easetype", iTween.EaseType.linear));
			}
			GameObject.Destroy (this.gameObject);
		}
	}
}
