﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class TriggerStudentGoblin2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void OnTriggerEnter2D(Collider2D other){
		if (other.name == "LaserGoblin") {
			GetComponentInChildren<SkeletonAnimation>().state.SetAnimation (0, "Morte", false);
			GameObject.Destroy (other.gameObject);
			SoundController.PlaySoundStudent (SoundStudentType.PlatformDeath, this.transform.position);
		}
	}
}
