﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class EnemyThornTEMP : MonoBehaviour {

	private SkeletonAnimation skeletonAnimation;

	public float cdToAttack = 5;

	public string tagPlayer;

	public DeathType deathType;

	public bool canGo = true;

	private BoxCollider2D boxCollider;
	private Vector3 originalPosition;

	void Start() {
		skeletonAnimation = GetComponentInChildren<SkeletonAnimation> ();
		boxCollider = GetComponent<BoxCollider2D> ();
		originalPosition = boxCollider.transform.position;
		if (skeletonAnimation.state.ToString () == "InimigoEspinhoGeloIdleFechado") {
			canGo = true;
		} 
		else {
			canGo = false;
		}
	}
		
	public void  FixedUpdate () {
		if (boxCollider.transform.position.x >= originalPosition.x) {
			boxCollider.transform.position -= new Vector3(.0001f,0,0);
		}
		else {
			boxCollider.transform.position += new Vector3(.0001f,0,0);
		}


		if (canGo) {
			if (skeletonAnimation.state.ToString () == "InimigoEspinhoGeloIdleFechado") {
				skeletonAnimation.state.SetAnimation (0, "InimigoEspinhoGeloAbre", false);
				SoundController.PlaySoundEnemy (SoundEnemyType.EnemyThorn, transform.position);
			}
			else if (skeletonAnimation.state.GetCurrent (0) == null) {
				Invoke ("CooldownShot", cdToAttack);
				ChangeAnimationState ("InimigoEspinhoGeloIdleAberto");
			}
		} 
		else {
			if (skeletonAnimation.state.ToString () == "InimigoEspinhoGeloIdleAberto")
				skeletonAnimation.state.SetAnimation (0, "InimigoEspinhoGeloFecha", false);
			else if (skeletonAnimation.state.GetCurrent (0) == null) {
				Invoke ("CooldownShot", cdToAttack);
				ChangeAnimationState ("InimigoEspinhoGeloIdleFechado");
			}
		}
	}

	void CooldownShot(){
		if (!canGo)
			canGo = true;
		else
			canGo = false;
	}

	public void ChangeAnimationState(string animation){
		skeletonAnimation.state.SetAnimation(0, animation, true);
	}

	void OnTriggerStay2D (Collider2D other) {
		if (other.tag == tagPlayer && canGo) {
			Death death = other.GetComponent<Death> ();

			death.KillCharacter (deathType);
		}
	}
}
