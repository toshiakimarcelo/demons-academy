
PlacaFinalVoadora.png
format: RGBA8888
filter: Linear,Linear
repeat: none
placa_fimdejogoAsa1
  rotate: false
  xy: 2, 482
  size: 376, 224
  orig: 376, 224
  offset: 0, 0
  index: -1
placa_fimdejogoAsa1_2
  rotate: false
  xy: 2, 242
  size: 235, 238
  orig: 235, 238
  offset: 0, 0
  index: -1
placa_fimdejogoAsa1_3
  rotate: true
  xy: 239, 245
  size: 235, 209
  orig: 235, 210
  offset: 0, 1
  index: -1
placa_fimdejogoAsa2
  rotate: false
  xy: 505, 760
  size: 375, 224
  orig: 376, 224
  offset: 0, 0
  index: -1
placa_fimdejogoAsa2_2
  rotate: false
  xy: 2, 2
  size: 235, 238
  orig: 235, 238
  offset: 0, 0
  index: -1
placa_fimdejogoAsa2_3
  rotate: true
  xy: 239, 9
  size: 234, 209
  orig: 235, 209
  offset: 0, 0
  index: -1
placa_fimdejogoCauda1
  rotate: true
  xy: 505, 710
  size: 48, 90
  orig: 48, 90
  offset: 0, 0
  index: -1
placa_fimdejogoCauda2
  rotate: false
  xy: 882, 890
  size: 71, 94
  orig: 71, 94
  offset: 0, 0
  index: -1
placa_fimdejogoCauda3
  rotate: false
  xy: 955, 899
  size: 67, 85
  orig: 67, 85
  offset: 0, 0
  index: -1
placa_fimdejogoLousa
  rotate: false
  xy: 2, 708
  size: 501, 276
  orig: 501, 276
  offset: 0, 0
  index: -1
