﻿using UnityEngine;
using System.Collections;

public class ImmunityDeathConfig : MonoBehaviour {

	private EnemyBitter enemybitter;

	// Use this for initialization
	void Start() {
		GetComponentInParent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "EnemyBitter") {
			
			enemybitter = other.GetComponent<EnemyBitter> ();
			enemybitter.GetComponentInParent<CircleCollider2D> ().enabled = false;
			enemybitter.GetComponentInParent<TriggerEnemyBitter> ().chase = false;
			enemybitter.gameObject.SetActive (false);

		}

		if (other.tag == "Player") {
			other.tag = "NoPlayer";
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == "NoPlayer") {
			other.tag = "Player";
		}
	}

	void OnDestroy (){
		enemybitter.GetComponentInParent<CircleCollider2D> ().enabled = true;
		enemybitter.gameObject.SetActive (true);
		enemybitter.startAgain = true;
	}
}
