﻿using UnityEngine;
using System.Collections;

public class WormosuActive : MonoBehaviour {

	public GameObject[] wormosu;
	public GameObject boggy;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerStay2D (Collider2D other) {
		if (other.tag == "Player") {
			boggy.GetComponent<Respawn> ().enabled = true;
			for (int i = 0; wormosu.Length > i; i++) {
				wormosu [i].SetActive (true);
			}
			SoundController.PlayBGM (SoundTypeBGM.BossWormosu1);
			Destroy (this.gameObject);
		}
	}
}
