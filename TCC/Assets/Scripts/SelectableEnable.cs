﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectableEnable : MonoBehaviour {

	public GameObject button;

	public bool firstTime = true;

	public bool go;

	void OnEnable() {
		go = true;
	}

	void Update() {
		if (this.gameObject.name == "Main Menu" && firstTime) {
			firstTime = false;
		} else if (go) {
			//button.GetComponent<Selectable> ().Select ();
			Invoke ("WaitASecond", .005f);
			go = false;
		}
	}

	void WaitASecond() {
		button.GetComponent<Selectable> ().Select ();
	}
}