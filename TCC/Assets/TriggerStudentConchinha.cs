﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class TriggerStudentConchinha : MonoBehaviour {

	public GameObject student;
	public string initialAnim;

	void Update () {
		if (student.GetComponentInChildren<SkeletonAnimation>().state.ToString () == initialAnim) {
			student.GetComponentInChildren<SkeletonAnimation>().state.SetAnimation (0, "JumpDeath", false);
			Invoke ("Delay", .5f);

		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			this.enabled = true;
		}
	}

	void Delay() {
		SoundController.PlaySoundStudent (SoundStudentType.JumpDeath, this.transform.position);
		GameObject.Destroy (this.gameObject);
	}

}
