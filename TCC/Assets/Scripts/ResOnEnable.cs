﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResOnEnable : MonoBehaviour {

	public Toggle toggle;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable() {
		if (PlayerPrefs.HasKey ("res")) {
			toggle.isOn = PlayerPrefs.GetInt ("res") == 0 ? false : true;
		}
	}
}
