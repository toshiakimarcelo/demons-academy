﻿using UnityEngine;
using System.Collections;

public enum EaseType {None, In, Out, InOut}

public class Wormosu : MonoBehaviour {

	public float maxUp;
	public float speed;

	public Transform playerPos;

	private Vector3 startPos;
	private Vector2 desiredPos;
	public float currentlerpTime;
	public float lerpTime;
	public bool goingUp = true;
	//float t = 0;
	float velX;
	public GameObject warning;

	public GameObject[] wormosuBody;

	public float Ease (float t, EaseType easeType){
		if (easeType == EaseType.None)
			return t;
		else if (easeType == EaseType.In)
			return Mathf.Lerp (0, 1, 1 - Mathf.Cos (t * Mathf.PI * .5f));
		else if (easeType == EaseType.Out)
			return Mathf.Lerp (0, 1, Mathf.Sin (t * Mathf.PI * .5f));
		else
			return Mathf.SmoothStep (0, 1, t);
	}

	public EaseType easeType = EaseType.Out;

	// Use this for initialization
	void Start () {
		warning.SetActive (false);
		startPos = transform.position;
		desiredPos = new Vector2 (playerPos.transform.position.x, startPos.y + maxUp);
		float distanceX = Vector2.Distance(new Vector2(this.transform.position.x,0), new Vector2(playerPos.position.x,0));
		warning.transform.position = new Vector2 (desiredPos.x - distanceX/2 - 1, warning.transform.position.y);
		SoundController.PlaySoundBoss (SoundBossType.BossEffect1);
		velX = 20;
//		desiredSpeed = 0;
//		StartCoroutine(Lerp ());
//		StartCoroutine(LerpX ());
    }

//	IEnumerator Lerp(){
//		float t = 0;
//		while (t < 1) {
//			t += Time.deltaTime * .1f;
//			transform.position = new Vector2(this.transform.position.x,Mathf.Lerp(this.transform.position.y,desiredPos.y, Ease(t,easeType)));
//			yield return new WaitForEndOfFrame();
//		}
//	}

//	IEnumerator LerpX(){
//		float t = 0;
//		while (t < 10) {
//			t += Time.deltaTime * .025f;
//			transform.position = new Vector2(Mathf.Lerp(this.transform.position.x,desiredPos.x, Ease(t,EaseType.None)),this.transform.position.y);
//			yield return new WaitForEndOfFrame();
//		}
//	}
//	
	// Update is called once per frame
	void Update () {
		currentlerpTime += Time.deltaTime;
		if (currentlerpTime > lerpTime) {
			currentlerpTime = lerpTime;
		}
//		if (currentlerpTime <= .05f * lerpTime) {
//			desiredPos = new Vector2 (playerPos.position.x, desiredPos.y);
//		}


		transform.position = new Vector2(Mathf.MoveTowards(this.transform.position.x,desiredPos.x,velX*Time.deltaTime),this.transform.position.y);

		float t = currentlerpTime / lerpTime;
		 t = 1 - Mathf.Cos (t * Mathf.PI * 0.5f);
		//t = Mathf.Sin (t * Mathf.PI * 0.5f);
//		this.transform.position = new Vector2(this.transform.position.x,Mathf.Lerp(this.transform.position.y,desiredPos.y, t));


		float distance = Vector2.Distance(new Vector2(0,this.transform.position.y), new Vector2(0,desiredPos.y));
//		if (distance <= 0.5f) {
//			StopCoroutine ("Lerp");
//			desiredPos = new Vector2 (playerPos.transform.position.x, startPos.y);
//			StartCoroutine(Lerp ());
//			easeType = EaseType.In;
//		}

//
//		float distance = Vector2.Distance(this.transform.position, desiredPos);
		if (distance >= 1 && goingUp) {
			this.transform.position = new Vector2(this.transform.position.x,Mathf.Lerp(this.transform.position.y,desiredPos.y, t*Time.deltaTime*speed));
		} 
		else if (distance >= 1 && !goingUp) {
			this.transform.position = new Vector2(this.transform.position.x,Mathf.Lerp(this.transform.position.y,desiredPos.y, t*Time.deltaTime*speed));
		} 
		else {
			if (goingUp) {
				if (playerPos != null) desiredPos = new Vector2 (playerPos.position.x, startPos.y);
				else desiredPos = new Vector2 (desiredPos.x, startPos.y);
				goingUp = false;
				warning.SetActive (false);
				SoundController.PlaySoundBoss (SoundBossType.BossEffect2);
			} 
			else if (playerPos != null) {
				SoundController.PlaySoundBoss (SoundBossType.BossEffect1);
				goingUp = true;
				float distanceX = 0;
				distanceX = Vector2.Distance(new Vector2(this.transform.position.x,0), new Vector2(playerPos.position.x,0));
				if (distanceX < 2) {
					distanceX = 2;
				}

				if (this.transform.position.x >= playerPos.position.x) {
					desiredPos = new Vector2 (playerPos.position.x, startPos.y + maxUp);
					warning.transform.position = new Vector2 (desiredPos.x + distanceX/4*3, warning.transform.position.y);
					for (int i = 0; i < wormosuBody.Length; i++) {
						wormosuBody [i].transform.localScale = new Vector3 (-1, 1, 1);
					}
				} 
				else {
					desiredPos = new Vector2 (playerPos.position.x, startPos.y + maxUp);
					warning.transform.position = new Vector2 (desiredPos.x - distanceX/4*3, warning.transform.position.y);
					for (int i = 0; i < wormosuBody.Length; i++) {
						wormosuBody [i].transform.localScale = new Vector3 (1, 1, 1);
					}
				}
				warning.SetActive (true);

				velX = distanceX;
			}

			currentlerpTime = 0;
		}

	}




}

