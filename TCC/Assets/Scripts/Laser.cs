﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {

	private Rigidbody2D body2d;

	public string tagPlayer;
	public string tagCameraLimit;
	public string tagIcePlatform;

	public DeathType deathType;
	public float speed;

	private Vector2 dir;
	private Vector2 normal;
	private Vector2 startPoint;

	private Vector2 initialPosition;

	[HideInInspector] public bool canGo = true;

	void Awake() {
		initialPosition = transform.position;
		body2d = GetComponent<Rigidbody2D> ();
	}

	void Update () {
		if (!canGo) {
			transform.position = Vector2.MoveTowards (transform.position, new Vector2 (transform.position.x + dir.x, transform.position.y + dir.y), speed * Time.deltaTime);
		} 
		else {
			transform.position = initialPosition;
		}
	}

	public void StartLaser() {
		dir = transform.right;
		startPoint = transform.position;
	}



	void OnCollisionEnter2D (Collision2D other) {
		if (other.collider.tag == tagPlayer) {
			DeathPlayer (other.gameObject);
		}
		else if (other.collider.tag == tagIcePlatform) {
			foreach (ContactPoint2D contact in other.contacts) {
				normal = contact.normal;
			} 
			dir = Vector2.Reflect (dir.normalized, normal);

			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
			startPoint = other.transform.position;

		} 
		else {
			canGo = true;
		}
	}

	void OnTriggerExit2D  (Collider2D other) {
		if (other.tag == tagCameraLimit) {
			canGo = true;
		} 
	}

	void DeathPlayer(GameObject other) {
		Death death = other.GetComponent<Death> ();

		death.KillCharacter (deathType);

		SoundController.PlaySound (SoundType.PlatformDeath);

		canGo = true;
	}
}
