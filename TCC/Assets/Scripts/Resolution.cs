﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Resolution : MonoBehaviour {

	public List<int> resWidth;
	public List<int> resHeight;
	public Dropdown dropdownMenu;
	public Text text;
	public GameObject options;

	void Start()
	{
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			int actualResWidth = Screen.width;
			int actualResHeight = Screen.height;
			if (Screen.resolutions [i].width >= 800) {
				resWidth.Add (Screen.resolutions [i].width);
				resHeight.Add (Screen.resolutions [i].height);

				dropdownMenu.options.Add (new Dropdown.OptionData () { text = ResToString (i) });
				dropdownMenu.value = i;

				dropdownMenu.onValueChanged.AddListener (delegate {
					PlayerPrefs.SetInt ("resWidth", resWidth[dropdownMenu.value]); 
					PlayerPrefs.SetInt ("resHeight", resHeight[dropdownMenu.value]); 
					Screen.SetResolution(resWidth[dropdownMenu.value], resHeight[dropdownMenu.value], (PlayerPrefs.GetInt("res") ==  0) ? false : true);
				});
			}
			Screen.SetResolution(actualResWidth, actualResHeight, (PlayerPrefs.GetInt("res") ==  0) ? false : true);
		}
		options.GetComponent<RectTransform> ().gameObject.SetActive (false);
	}

	string ResToString (int value) {
		return Screen.resolutions[value].width + " x " + Screen.resolutions[value].height;
	}
}
