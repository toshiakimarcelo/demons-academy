﻿using UnityEngine;
using System.Collections;

public class TriggerStudentFishing : MonoBehaviour {


	
	public GameObject student;
	public GameObject explosion;


	void OnTriggerEnter2D(Collider2D other){
		if (other.name == student.name) {
			
			explosion.transform.position = other.transform.position;
			explosion.SetActive (true);
			SoundController.PlaySoundStudent (SoundStudentType.TreeDeath, this.transform.position);
			GameObject.Destroy (other.GetComponentInParent<RectTransform> ().gameObject);
		}
	}
}
