﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class TriggerStudentConcha1 : MonoBehaviour {

	public GameObject wormosu;
	public string path;
	public float time;

	public GameObject student;
	public Rigidbody2D tree;
	public Rigidbody2D peso;

	private bool startShake;
	public float shakeAmt;
	public float timeShaking;
	private float originalPpX;
	private float originalPpY = 0;

	void Update() {
		if (startShake) {
			InvokeRepeating ("CameraShake", 0, 0.01f);
			Invoke ("StopShaking", timeShaking);
		}
	}


	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			student.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Olha pra cima", false);
			tree.constraints = RigidbodyConstraints2D.None;
			peso.constraints = RigidbodyConstraints2D.None;
			iTween.MoveTo (wormosu, iTween.Hash ("path", iTweenPath.GetPath (path), "time", time, "easetype", iTween.EaseType.linear));
			startShake = true;
			originalPpX = Camera.main.GetComponent<CameraControllerV3> ().distFromTargetX;
			SoundController.PlaySoundStudent (SoundStudentType.Wormosu1, this.transform.position);
			this.enabled = true;
		}
	}

	void StopShaking() {
		startShake = false; 
		Camera.main.GetComponent<CameraControllerV3> ().distFromTargetY = 0;
		Camera.main.GetComponent<CameraControllerV3> ().distFromTargetX = originalPpX;
		GameObject.Destroy (this.gameObject);
	}

	void CameraShake(){
		float quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;
		float ppy = Camera.main.GetComponent<CameraControllerV3> ().distFromTargetY;
		float ppx = Camera.main.GetComponent<CameraControllerV3> ().distFromTargetX;
		ppy += quakeAmt;
		ppx += quakeAmt;
		Camera.main.GetComponent<CameraControllerV3> ().distFromTargetY = ppy;
		Camera.main.GetComponent<CameraControllerV3> ().distFromTargetX = ppx;
	}
}
