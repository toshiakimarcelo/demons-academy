﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AudumblaLaser : MonoBehaviour {


	public GameObject bodyBoggy;
	private Rigidbody2D body2d;

	public string tagPlayer;
	public string tagCameraLimit;
	public string tagIcePlatform;

	public DeathType deathType;
	public float speed;

	private Vector2 dir;
	private Vector2 normal;
	private Vector2 startPoint;

	private Vector2 initialPosition;

	public bool playerDied;

	public GameObject explosionPlayer;

	public GameObject player;

	public GameObject explosionAudumbla;

	[HideInInspector] public bool canGo = true;

	private Color originalColor;

	public Color damageColor;

	void Awake() {
		initialPosition = transform.position;
		body2d = GetComponent<Rigidbody2D> ();
		originalColor = GetComponent<SpriteRenderer> ().color;
	}

	void FixedUpdate () {
		if (!canGo) {
			transform.position = Vector2.MoveTowards (transform.position, new Vector2 (transform.position.x + dir.x, transform.position.y + dir.y), speed * Time.deltaTime);
		} 
		else {
			transform.position = initialPosition;
			GetComponent<SpriteRenderer> ().color = originalColor;
		}
	}

	public void StartLaser() {
		dir = transform.right;
		startPoint = transform.position;
	}



	void OnCollisionEnter2D (Collision2D other) {
		if (other.collider.tag == tagPlayer) {
			if (!playerDied) {
				StartCoroutine (GameOver ());
				playerDied = true;
			}
		}
		else if (other.collider.tag == tagIcePlatform) {
			foreach (ContactPoint2D contact in other.contacts) {
				normal = contact.normal;
			} 
			dir = Vector2.Reflect (dir.normalized, normal);

			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
			startPoint = other.transform.position;
			GetComponent<SpriteRenderer> ().color = damageColor;

		} 
		else {
			canGo = true;
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Audumbla" && GetComponent<SpriteRenderer>().color != originalColor) {
			other.GetComponent<Audumbla> ().destroy = true;
			other.GetComponent<Audumbla> ().Damage ();
			canGo = true;

		}
	}

	void OnTriggerExit2D  (Collider2D other) {
		if (other.tag == tagCameraLimit) {
			canGo = true;
		} 
	}

	IEnumerator GameOver() {
		explosionPlayer.transform.position = player.transform.position;
		Death death = player.GetComponent<Death>();
		death.KillCharacter (DeathType.Deafult);
		GameObject.Destroy (player);
		yield return new WaitForSeconds (.5f);
		SoundController.PlaySound (SoundType.BossDeath);
		Camera.main.GetComponent<CameraControllerV3> ().target = bodyBoggy.transform;
		explosionPlayer.SetActive (true);
		yield return new WaitForSeconds (5);
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}

}
