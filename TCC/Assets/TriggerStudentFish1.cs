﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class TriggerStudentFish1 : MonoBehaviour {

	public GameObject wormosu;
	public string path;
	public float time;

	public GameObject student;

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			student.GetComponentInChildren<SkeletonAnimation> ().state.SetAnimation (0, "Susto", false);
			student.transform.localScale = new Vector3 (-.6f, .6f, 1);
			iTween.MoveTo (wormosu, iTween.Hash ("path", iTweenPath.GetPath (path), "time", time, "easetype", iTween.EaseType.linear));
			SoundController.PlaySoundStudent (SoundStudentType.Wormosu2, this.transform.position);
			GameObject.Destroy (this.gameObject);
		}
	}
}
